#!/usr/bin/env bash
loop_directory_and_install(){
for file in $1/*
do
temp_file_name=$(basename ${file})
if [[ -f ${file} ]] ; then
if [[ ${temp_file_name} == "requirements.txt" ]] ; then
echo " "
echo "--------------- Installing requirements in $(basename $1) ---------------"
echo " "
pip install --ignore-installed -r ${file}
elif [[ ${temp_file_name} == "config.ini.example" ]] ; then
mv ${file} $1/config.ini
fi
elif [[ -d ${file} ]] ; then
if [[ ${temp_file_name} != "venv" ]] ; then
loop_directory_and_install ${file}
fi
fi
done
}
echo "--------------- Installing virtualenv ---------------"
pip install virtualenv
virtualenv venv
source venv/bin/activate
current_directory=$(echo $PWD)
loop_directory_and_install "$current_directory"
echo "------------------------- Project setup finished  -------------------------"
deactivate
