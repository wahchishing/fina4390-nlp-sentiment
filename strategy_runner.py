from strategy_db import *
from strategy_script import *
import traceback

TARGET_STRATEGY_DICT = modi_strategy_1
CSV_NAME_LIST = list(filter(lambda x: 'aastock' not in x and 'expanded' not in x and 'bloomberg' not in x and 'full' not in x, os.listdir(os.path.join(PROJECT_DIR, 'data', 'aastock'))))
STRATEGY_LIST = [
    modi_strategy_1
    # strategy_2,
    # strategy_3,
    # strategy_4
]

"""
TARGET_STRATEGY contains the following k-v pairs:
    - 'strategy_name':  'strategy 4',
    - 'strategy_func':   strat_4,
    - 'params_dict':     strat_4_params_dict,
    - 'param_dict':      strat_4_param_dict
"""


if __name__ == '__main__':

    # result_list = []
    # error_list = []
    # for csv_name in CSV_NAME_LIST:
    #     for strategy in STRATEGY_LIST:
    #         runner = StrategyTester(strategy, csv_name)
    #         input_dict = {
    #             'csv_name': csv_name,
    #             'strategy': strategy['strategy_name']
    #         }
    #         [p1_name, p1_list], [p2_name, p2_list] = strategy['params_dict'].items()
    #         for p1 in p1_list:
    #             for p2 in p2_list:
    #                 target_param_dict = {
    #                     p1_name: p1,
    #                     p2_name: p2
    #                 }
    #                 log_message('Start testing: | strategy: {} | CSV: {} | {}: {} | {}: {}'.format(strategy['strategy_name'], csv_name, p1_name, p1, p2_name, p2))
    #                 input_dict['parameters'] = target_param_dict.copy()
    #                 try:
    #                     input_dict['result_metrics'] = runner.test_strategy(target_param_dict, save=False)
    #                 except Exception as e:
    #                     log_message('Error there: Skipping one test')
    #                     error_list.append(input_dict.copy())
    #                 result_list.append(input_dict.copy())

    csvv = 'vader_HSI_score_1600_1600_m.csv'
    results = {}
    runner = StrategyTester(modi_strategy_1, csvv)
    results[csvv] = runner.plot_strategy_metrics()
