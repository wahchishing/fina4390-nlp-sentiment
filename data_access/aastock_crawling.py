from selenium import webdriver
import pandas as pd
import os
import datetime
import time
import requests
import jsons
from data_access.util_functions import *
from bs4 import BeautifulSoup

# Mapping for keywords and language
lang_word_map = {
	'en': {
		'r': 'Recommend',
		'p': 'Positive',
		'n': 'Negative'
	},
	'tc': {
		'r': '推薦',
		'p': '利好',
		'n': '利淡'
	},
	'sc': {
		'r': '推荐',
		'p': '利好',
		'n': '利淡'
	}
}


def parse_row(text, title, lang='en'):
	r_word = lang_word_map[lang]['r']
	p_word = lang_word_map[lang]['p']
	n_word = lang_word_map[lang]['n']
	return_dict = {
		'title': title,
		'language': lang,
		'news_time': remove_head_tail_white_space(text[:text.index(r_word)]),
		'recommend': text[text.index(r_word) + len(r_word):text.index(p_word)],
		'positive': text[text.index(p_word) + len(p_word):text.index(n_word)],
		'negative': text[text.index(n_word) + len(n_word):],
		'scrape_time': str(datetime.datetime.today())[:-3]
	}
	return return_dict


def remove_head_tail_white_space(input_str):
	if input_str[0] == ' ':
		input_str = input_str[1:]
	if input_str[-1] == ' ':
		input_str = input_str[:-1]
	return input_str


# Available option for news_page:
news_page_type = [
	'popular-news',
	'latest-news',
	'analysts-views'
]

language_type = [
	'en',
	'sc',
	'tc'
]


def get_aastock_news(news_page='popular-news', language='en'):
	"""
	:param news_page: "popular-news", "latest-news" or "analysts-views"
	:param language: "en", "sc" or "tc"
	:return: a list of dictionaries storing text data
	"""
	if news_page not in news_page_type:
		log_message('Please choose from available news page type', 'WrongNewsPageType')
		return None
	if language not in language_type:
		log_message('Please choose from available language type', 'WrongLanguageType')
		return None
	chrome = open_chrome()
	url = 'http://www.aastocks.com/{}/stocks/news/aafn/{}'.format(language, news_page)
	chrome.get(url)
	scroll_to_bottom(chrome, jump_height=200, time_gap=0.2)
	rows = chrome.find_element_by_id('aafn-search-c1').text.split('\n')
	index_list = list(filter(lambda x: x != 't', [rows.index(i) if i[:4] == '2020' else 't' for i in rows]))
	input_list = [[rows[i], rows[i - 1]] for i in index_list]
	dict_list = [parse_row(i[0], i[1], language) for i in input_list]
	log_message('{} data ready'.format(news_page.upper()))
	chrome.close()
	return dict_list


def parse_company_news(news, language):
	return_dict = {
		'news_id': news.get('id', None),
		'news_dtd': news.get('dtd', None),
		'language': language,
		'scrape_time': str(datetime.datetime.today())[:-3],
		'news_time': datetime.datetime.strptime(news.get('dt', None), "%Y/%m/%d %H:%M"),
		'title': news.get('h', None),
		'positive': news.get('bucnt', None),
		'negative': news.get('becnt', None),
		'recommend': news.get('rcnt', None),
		'company_report': news.get('tdesp', None)
	}
	return return_dict


sample_param_dict = {
	'ticker': '00700',
	'num_days': 50,
	'language': 'en',
	'get_content': True,
	'parallel': True
}


def get_aastock_company(param_dict=None):
	if param_dict is None:
		param_dict = sample_param_dict
	ticker = param_dict['ticker']
	num_days = param_dict['num_days']
	language = param_dict['language']
	get_content = param_dict['get_content']
	parallel = param_dict['parallel']
	log_message('Working on {}'.format(ticker))
	if language not in language_type:
		log_message('Please choose from available language type', 'WrongLanguageType')
		return None
	last_newstime = 99999999999
	last_datetime = datetime.datetime.now()
	target_datetime = datetime.datetime.now() - datetime.timedelta(num_days)
	dict_list = []
	while last_datetime > target_datetime:
		url = f'http://www.aastocks.com/{language}/resources/datafeed/getmorenews.ashx?cat=company-news&newstime={last_newstime}&period=0&key=&symbol={ticker}'
		rq = requests.get(url)
		try:
			news = jsons.loads(rq.text)
			dict_list += [parse_company_news(n, language) for n in news]
			last_datetime = dict_list[-1]['news_time']
			last_newstime = dict_list[-1]['news_dtd']
		except:
			log_message(f'AASTOCK: {ticker} ended prematurely')
			break
	if get_content:
		log_message('Start getting news content')
		dict_list = list(filter(lambda x: x['company_report'] is None, dict_list))
		news_id_list = [i['news_id'] for i in dict_list]
		url_list = ['http://www.aastocks.com/{}/stocks/news/aafn-con/{}/company-news'.format(language, i) for i in news_id_list]
		if parallel:
			return_list = list(parallel_helper(get_one_news_content, url_list))
		else:
			return_list = list(map(get_one_news_content, url_list))
		log_message('Got all news content')
		foo = [dict_list[i].update({'news_content': return_list[i], 'stock_code': ticker}) for i in range(return_list.__len__())]
	log_message(f'AASTOCK: {ticker} data ready')
	return dict_list


def get_one_news_content(target_url):
	try:
		# log_message('Getting ' + target_url)
		ret = requests.get(target_url)
		soup = BeautifulSoup(ret.text, 'lxml')
		content = soup.find('p')
		content_text = content.text
		content_text = content_text.replace('\xa0', '')
		if content_text == '':
			content_text = soup.find('title').text.replace('\r*', '').replace('\r', '').replace('\n', '')
		# do all the filtering here
	except Exception as e:
		# log_message('Error')
		return 'Error'
	log_message('Got ' + target_url)
	return content_text


