import tweepy
from botometer import Botometer
import datetime
import pandas as pd
import json
import tabulate
from data_access.util_functions import *

twitter_api_keys = {
    'consumer_api_key': "8ay2XYcI4Le9r7bMpJ2UDPZDw",
    'consumer_api_secret_key': "m4jacEm1j9pNSZ8MRGS72igqraqhnBTzyCxoLsyfSKI2QDFBy3",
    'access_token_key': "917762567577133057-HDdBtIvJUgLIS5IKexrLEfu2FONumOD",
    'access_token_secret_key': "gWOmLSEIOyVSHYUlUPV2kzhIA0P7964dCa9U0ytYZma4N"
}

rapid_api_key = {
    'api_host': 'osome-botometer.p.rapidapi.com',
    'api_key': '79cdab6a65msh26af1cdca18fc64p166b4ejsnf071c600a37e'
}


class TweepyHelper:
    def __init__(self):
        self.api_object = create_tweepy_api()

    def get_tweet_search(self,
                         target_text: str,
                         return_type: str = None,
                         cursor: bool = False,
                         count: str = 10000,
                         tweet_mode: str = 'extended',
                         language: str = 'en',
                         date_to: str = None,
                         days: int = None):
        """
        Get the tweets that includes the target_text, e.g.: "$HSI"
        :param target_text:       add a "$" sign for stocks/index mentions
        :param return_type:       'df' or 'list' or None
        :param cursor:            True or False
        :param count:             max. number of tweets to get
        :param tweet_mode:        I forget what this is lollll
        :param language:          default 'en'
        :param date_to:           in format of "%Y-%m-%d", default today
        :param days:              number of days from today if date_to is None, default 7
        :return:                  df or list storing the search results
        """
        if date_to is None:
            date_to = datetime.datetime.today().date().strftime("%Y-%m-%d")
        if days is None:
            days = 7
        date_from = (datetime.datetime.today() - datetime.timedelta(days=days)).strftime("%Y-%m-%d")
        if not cursor:
            tweets_search_result = self.api_object.search(q=target_text + ' -filter:retweets',
                                                          since=date_from,
                                                          until=date_to,
                                                          count=count,
                                                          tweet_mode=tweet_mode,
                                                          language=language)
        else:
            tweets_search_result = tweepy.Cursor(self.api_object.search,
                                                 q=target_text + ' -filter:retweets',
                                                 since=date_from,
                                                 until=date_to,
                                                 count=count)
        if return_type == 'df':
            if cursor:
                return_df = cursor_return_df(tweets_search_result)
                if return_df.empty:
                    return return_df
                if language is not None:
                    return return_df.loc[return_df['language'] == language]
                return return_df
            return tweets_to_df(tweets_search_result)
        elif return_type == 'list':
            if cursor:
                return_list = tweets_to_list(tweets_search_result, 'cursor')
                if language is not None:
                    return_list = list(filter(lambda x: x['language'] == language, return_list))
                return return_list
            return tweets_to_list(tweets_search_result)
        else:
            return tweets_search_result

    def get_user_search(self,
                        screen_name,
                        return_type=None,
                        tweet_mode='extended'):
        tweets_by_user = self.api_object.user_timeline(screen_name=screen_name,
                                                       tweet_mode=tweet_mode)
        if return_type == 'df':
            return tweets_to_df(tweets_by_user)
        if return_type == 'list':
            return tweets_to_list(tweets_by_user)
        else:
            return tweets_by_user


class BotometerHelper:
    def __init__(self):
        self.botometer_obj = create_botometer_api()

    def get_bot_score(self, account_name):
        log_message('Checking ' + account_name)
        bot_score = self.botometer_obj.check_account(account_name)
        log_message('Finished checking ' + account_name)
        return bot_score

    def is_bot(self, account_name):
        target_score = self.get_bot_score(account_name)
        if target_score['scores']['english'] > 0.6:
            log_message(account_name + ' is bot')
            return True
        else:
            log_message(account_name + ' is not bot')
            return False


############################################### Helper Functions ###############################################


def create_tweepy_api():
    my_api_keys = twitter_api_keys
    my_consumer_key = my_api_keys['consumer_api_key']
    my_consumer_secret_key = my_api_keys['consumer_api_secret_key']
    auth = tweepy.AppAuthHandler(my_consumer_key, my_consumer_secret_key)
    return_api_object = tweepy.API(auth,
                                   wait_on_rate_limit=True,
                                   wait_on_rate_limit_notify=True)
    return return_api_object


def create_botometer_api():
    rapid_key = rapid_api_key
    twitter_tokens = twitter_api_keys
    botometer_obj = Botometer(mashape_key=rapid_key,
                              **twitter_tokens,
                              wait_on_ratelimit=False)
    return botometer_obj


def tweet_to_json(tweet_result, input_type='normal'):
    if input_type == 'cursor':
        return {
            'tweet_id': tweet_result.id_str,
            'created_at_utc': str(tweet_result.created_at),
            'user_screen_name': tweet_result.user.screen_name,
            'tweet_text': tweet_result.text,
            'retweet_count': tweet_result.retweet_count,
            'favourite_count': tweet_result.favorite_count,
            'language': tweet_result.lang
        }
    return {
        'tweet_id': tweet_result.id_str,
        'created_at_utc': str(tweet_result.created_at),
        'user_screen_name': tweet_result.user.screen_name,
        'tweet_text': tweet_result.full_text,
        'retweet_count': tweet_result.retweet_count,
        'favourite_count': tweet_result.favorite_count,
        'language': tweet_result.lang
    }


def tweets_to_df(tweets_result):
    temp_list = tweets_to_list(tweets_result)
    return pd.DataFrame(temp_list)


def cursor_return_df(cursor_result_input):
    return_list = [tweet_to_json(tweet, 'cursor') for tweet in cursor_result_input.items()]
    return pd.DataFrame(return_list)


def print_tweet_info(tweet_result):
    tweet_info = [
        ['Tweet ID: ', tweet_result.id_str],
        ['Created At (UTC):', tweet_result.created_at],
        ['User Screen Name: ', tweet_result.user.screen_name],
        ['Tweet Text: ', tweet_result.full_text],
        ['Retweet Count:', tweet_result.retweet_count],
        ['Favourite Count:', tweet_result.favorite_count],
        ['Language:', tweet_result.lang],
    ]
    print(tabulate.tabulate(tweet_info))


def tweets_to_list(tweets_result, input_type='normal'):
    return [tweet_to_json(i, input_type) for i in tweets_result]
