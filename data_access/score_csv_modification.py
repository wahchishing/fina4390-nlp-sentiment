import os
import pandas as pd


PROJECT_DIR = os.path.abspath(__file__)[:os.path.abspath(__file__).find('nlp-sentiment') + 13]


def process_one_csv(csv_path):
    new_csv_path = csv_path.split('.')[0] + '_m.' + csv_path.split('.')[1]
    df = pd.read_csv(csv_path)
    for i, news in enumerate(df['news_content']):
        if news.find('Quote is delayed') == -1:
            continue
        indices = news.index('Quote is delayed')
        df.loc[i, 'news_content'] = news[:indices-1]

    for i, news in enumerate(df['news_content']):
        if news.find('AASTOCKS Financial News - Company News') == -1:
            continue
        indices = news.index('AASTOCKS Financial News - Company News')
        df.loc[i, 'news_content'] = news[:indices]

    for i, news in enumerate(df['news_content']):
        if not news or news == 'Error':
            df.loc[i, 'news_content'] = df.loc[i, 'title']

    for i, news in enumerate(df['news_content']):
        if news[0] == '*':
            df.loc[i, 'news_content'] = news[1:]
    df.to_csv(new_csv_path, index=False)
