from selenium import webdriver
import selenium
import numpy as np
import pandas as pd
from data_access.util_functions import *
import traceback
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys


DATE_LIST = list(np.concatenate([[f'{i}-01-01', f'{i}-07-01'] for i in range(2010, 2021)]))

PATH_TO_FILE = os.path.dirname(os.path.realpath(__file__))

CUHK_USER_NAME = '1155078304@link.cuhk.edu.hk'

CUHK_PWD = 'ABCabc.23638845'


def get_stock_name_xpath_list():
    return pd.read_csv(os.path.join(PATH_TO_FILE, 'factiva_details.csv'))


class FactivaNewsScrapper:
    def __init__(self):
        self.chrome = open_chrome()
        url = 'https://libguides.lib.cuhk.edu.hk/c.php?g=253932&p=1786025'
        self.chrome.get(url)
        # Login stuff here
        self.retry_click('//*[@id="s-lg-content-9419390"]/a')
        self.retry_click('//*[@id="container"]/center/form/input[3]')
        self.retry_click('//*[@id="userNameInput"]')
        self.chrome.find_element_by_xpath('//*[@id="userNameInput"]').send_keys(CUHK_USER_NAME)
        self.chrome.find_element_by_xpath('//*[@id="passwordInput"]').send_keys(CUHK_PWD)
        self.chrome.find_element_by_xpath('//*[@id="submitButton"]').click()
        self.retry_click('//*[@id="details-button"]')
        self.retry_click('//*[@id="proceed-link"]')
        self.retry_click('//*[@id="navmbm0"]/a')
        # while input('Login first, and enter "y" when you are at the search result page') != "y":
        #     continue
        log_message('Webpage ready')

    def retry_click(self, xpath):
        while 1:
            try:
                self.chrome.find_element_by_xpath(xpath).click()
                break
            except:
                time.sleep(1)
                continue
        return

    def custom_search(self, stock_name, stock_xpath, start_date, end_date):  # date: 'YYYY-MM-DD'
        start_d, start_m, start_y = start_date[-2:], start_date[-5:-3], start_date[:4]
        end_d, end_m, end_y = end_date[-2:], end_date[-5:-3], end_date[:4]
        time.sleep(2)
        self.retry_click('//*[@id="navmbm0"]/a')
        self.retry_click('//*[@id="coTab"]/div[1]')
        enter_company_elem = self.chrome.find_element_by_xpath('//*[@id="coTxt"]')
        enter_company_elem.clear()
        enter_company_elem.send_keys(stock_name)
        self.chrome.find_element_by_xpath('//*[@id="coLkp"]').click()
        while 1:
            try:
                self.chrome.find_element_by_xpath(stock_xpath).click()
                break
            except:
                try:
                    if 'again' in self.chrome.find_element_by_xpath('//*[@id="coMnu"]/div').text:
                        return 'Retry'
                    else:
                        time.sleep(0.2)
                except:
                    time.sleep(0.2)
                continue
        self.chrome.find_element_by_xpath('//*[@id="reTab"]/div[1]').click()
        enter_region_elem = self.chrome.find_element_by_xpath('//*[@id="reTxt"]')
        enter_region_elem.clear()
        enter_region_elem.send_keys('Hong Kong')
        self.chrome.find_element_by_xpath('//*[@id="reLkp"]').click()
        while 1:
            try:
                time.sleep(2)
                self.chrome.find_element_by_xpath('//*[@id="reMnu"]/ul/li[2]/a[1]').click()
                break
            except:
                time.sleep(0.2)
                continue
        date_choice = Select(self.chrome.find_element_by_xpath('//*[@id="dr"]'))
        date_choice.select_by_index(9)
        duplicate_choice = Select(self.chrome.find_element_by_xpath('//*[@id="isrd"]'))
        duplicate_choice.select_by_index(0)
        self.chrome.find_element_by_xpath('//*[@id="frd"]').clear()
        self.chrome.find_element_by_xpath('//*[@id="frd"]').send_keys(start_d)
        self.chrome.find_element_by_xpath('//*[@id="frm"]').send_keys(start_m)
        self.chrome.find_element_by_xpath('//*[@id="fry"]').send_keys(start_y)
        self.chrome.find_element_by_xpath('//*[@id="tod"]').send_keys(end_d)
        self.chrome.find_element_by_xpath('//*[@id="tom"]').send_keys(end_m)
        self.chrome.find_element_by_xpath('//*[@id="toy"]').send_keys(end_y)
        self.chrome.find_element_by_xpath('//*[@id="btnSBSearch"]/div/span').click()
        log_message(f'Search result for {stock_name} from {start_date} to {end_date} ready')
        return None

    def scrap_titles(self):
        if self.chrome.find_element_by_xpath('//*[@id="headlineTabs"]/table[1]/tbody/tr/td/span[2]/a').text == 'All(0)':
            return []
        next_page_xpath = '//*[@id="headlineHeader33"]/table/tbody/tr/td/a'
        master_content = []
        first_page = True
        get_one_item = '1'
        for i in range(100000):
            log_message(f'Start page {i + 1}')
            rows = self.chrome.find_element_by_xpath('//*[@id="headlines"]/table')
            ls = rows.text.split('\n')
            log_message('Getting table')
            """
            process your row here
            """
            master_content += ls
            log_message('Finished getting table')
            log_message('Go to next page')
            try:
                next_page = self.chrome.find_element_by_xpath(next_page_xpath)
            except:
                log_message('Scrap finished')
                break
            next_page.click()
            if first_page:
                get_one_item = self.chrome.find_element_by_xpath('//*[@id="headlines"]/table/tbody/tr[1]/td[2]').text
                next_page_xpath += '[2]'
                first_page = False
            old_item = get_one_item
            while 1:
                try:
                    time.sleep(4)
                    get_one_item = self.chrome.find_element_by_xpath('//*[@id="headlines"]/table/tbody/tr[1]/td[2]').text
                    if get_one_item == old_item:
                        pass
                        # log_message('Loading ... ')
                    else:
                        log_message('Reloaded')
                        break
                except Exception as e:
                    print(traceback.format_exc())
                    pass
        title_list = list(filter(lambda x: '.   ' in x[1], list(enumerate(master_content))))
        date_list = [master_content[x] for x in [i[0] + 1 for i in title_list]]
        title_list = [i[1].split('.   ')[1] for i in title_list]
        parsed_date_list = list(map(parse_one_date, date_list))
        fdate_list = list(map(format_one_date, parsed_date_list))
        return_list = list(filter(lambda x: x['date'] is not None and x['news_title'] is not None,
                                  [{'date': fdate_list[i], 'news_title': title_list[i]}
                                   for i in range(len(title_list))]))
        return return_list


def parse_one_date(one_date):
    try:
        if ':' not in one_date.split(', ')[1]:
            return one_date.split(', ')[1]
        else:
            return one_date.split(', ')[2]
    except:
        return None


def format_one_date(one_date):
    try:
        return str((datetime.datetime.strptime(one_date, '%d %B %Y')).date())
    except:
        return None


def loading_bar():
    pass


if __name__ == '__main__':
    stocks = list(pd.read_csv(os.path.join(PATH_TO_FILE, 'factiva_details.csv')).to_dict('index').values())
    done_stocks = os.listdir(os.path.join(PATH_TO_FILE, 'result_csvs'))
    done_stocks = [i[:-9] for i in done_stocks]
    stocks = list(filter(lambda x: x['stock_name'].replace(' ', '_') not in done_stocks, stocks))
    # stocks = [
    #     #     {
    #     #         'stock_name': 'hkex',
    #     #         'stock_xpath': '//*[@id="coMnu"]/ul/li/a[1]'
    #     #     }
    #     # ]
    # print(stocks)
    for stock in stocks:
        runner = FactivaNewsScrapper()
        result_list = []
        for i in range(len(DATE_LIST)):
            if i == 0:
                continue
            while 1:
                try:
                    log_message(f'Working on {DATE_LIST[ i - 1]} to {DATE_LIST[i]}')
                    a = runner.custom_search(stock["stock_name"], stock["stock_xpath"], DATE_LIST[i - 1], DATE_LIST[i])
                    if a == 'Retry':
                        runner.chrome.close()
                        runner = FactivaNewsScrapper()
                        continue
                    result_list += runner.scrap_titles()
                    log_message('Go to next')
                    break
                except Exception as e:
                    log_message('Error')
                    continue
        df = pd.DataFrame(result_list)
        df.drop_duplicates(subset='news_title', inplace=True)
        log_message(f'Got df for {stock["stock_name"]}')
        df.to_csv(f'result_csvs/{stock["stock_name"].replace(" ", "_")}_news.csv')
        runner.chrome.close()
    log_message('Finished')





