from selenium import webdriver
import requests
import bs4
import jsons
import datetime
import pandas as pd
from data_access.util_functions import *


def parse_story(story):
	return_dict = {
		'scrape_time': str(datetime.datetime.today())[:-3],
		'news_time': datetime.datetime.astimezone(story['updated_at']),
		'title': story['hed'],
		'abstract': story['lede'],
		'usn': story['usn'],
		'dateline': story['dateline']
	}
	return return_dict


def get_reuters_company(ticker='0001.HK', num_days=1000, cut_excess=False):
	"""
	:param ticker: in formate '0001.HK'
	:param num_days: number of days to crawl
	:return: a list of dictionary storing the data
	"""
	target_datetime = (datetime.datetime.now() - datetime.timedelta(num_days)).astimezone()
	last_datetime = datetime.datetime.now().astimezone()
	dict_list = []
	while last_datetime > target_datetime:
		url = f'https://wireapi.reuters.com/v8/feed/rcom/us/marketnews/ric:{ticker}?until={int(last_datetime.timestamp()) * 1000000000}'
		rq = requests.get(url)
		soup = bs4.BeautifulSoup(rq.text, "html.parser")
		try:
			d = jsons.loads(soup.text)
			for wireitems in d['wireitems']:
				for templates in wireitems['templates'][:-1]:
					try:
						dict_list.append(parse_story(templates['story']))
					except:
						pass
			last_datetime = dict_list[-1]['news_time']
		except:
			log_message(f'REUTERS: {ticker} data up to {last_datetime}')
			break
	if cut_excess:
		dict_list = list(filter(lambda d: d['news_time'] > target_datetime, dict_list))
	log_message(f'REUTERS: {ticker} data ready')
	return dict_list


if __name__ == '__main__':
	df = get_reuters_company()
	df = pd.DataFrame(df)
	print(df)


'''
chrome = webdriver.Chrome('chromedriver.exe')
url = 'https://www.reuters.com/companies/0700.HK'
chrome.get(url)
elems = chrome.find_elements_by_xpath('//*[@id="__next"]/div/div[4]/div[1]/div/div/div/div[5]')[0]
items = elems.find_elements_by_class_name('item')
dict_list = [{'title': i.text.split('\n')[0],
			  'abstract': i.text.split('\n')[1],
			  'news_time': i.text.split('\n')[2]
			  } for i in items]
df = pd.DataFrame(dict_list)
'''
