from yahoofinancials import YahooFinancials
import pandas as pd
from pandas.io.json import json_normalize
import datetime
from data_access.util_functions import *


"""
format:

HSI        : '^HSI'
equities   : '0700.HK', '0005.HK'
date       : YYYY-MM-DD
freq       : 'daily' | 'weekly' | 'monthly'   

"""


def get_hk_stock_price(stock_code, start_date=None, end_date=None, freq='daily'):
    if end_date is None:
        end_date = datetime.datetime.today().date()
    if start_date is None:
        start_date = datetime.date(end_date.year - 10, end_date.month, end_date.day)
    end_date = end_date.__str__()
    start_date = start_date.__str__()
    if stock_code == 'HSI' or stock_code == '^HSI':
        stock_code = '^HSI'
    else:
        stock_code = stock_code.zfill(7)
    yahoo_financials_get = YahooFinancials(stock_code)
    call_result_json = yahoo_financials_get.get_historical_price_data(start_date, end_date, freq)
    return_df = pd.DataFrame.from_dict(json_normalize(call_result_json[stock_code]['prices']), orient='columns')
    return_df.loc[:, 'date_time'] = return_df.loc[:,'formatted_date']
    return_df = return_df.loc[:, ['date_time', 'open', 'high', 'low', 'close', 'adjclose', 'volume']].dropna(axis=0)
    return_df = return_df.sort_values(by='date_time', ascending=True)
    return_df = return_df.reset_index(drop=True)
    log_message('Price df of {} ready'.format(stock_code))
    return return_df


if __name__ == '__main__':
    hi = get_hk_stock_price(stock_code='700.HK', start_date='2017-09-09', end_date=None)
