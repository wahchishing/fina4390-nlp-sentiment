import datetime
import os
from selenium import webdriver
import time
from contextlib import closing
from multiprocessing import Pool

path_to_file = os.path.dirname(os.path.realpath(__file__))


def log_message(message, err_message=None):
    if not err_message:
        log_str = '{}: {}'.format(str(datetime.datetime.today())[:-3], message)
    else:
        log_str = '{}: <<<ERROR:{}>>> {}'.format(str(datetime.datetime.today())[:-3], err_message, message)
    print(log_str)
    return None


def open_chrome():
    if os.name == 'posix':
        chrome = webdriver.Chrome(path_to_file + '/chromedriver')
    else:
        chrome = webdriver.Chrome(path_to_file + '/chromedriver.exe')
    return chrome


def scroll_to_bottom(driver, jump_height=1000, time_gap=0.1):
    last_height = driver.execute_script("return document.body.scrollHeight")
    y = 1
    while y * jump_height < last_height:
        driver.execute_script("window.scrollTo(0, " + str(y * jump_height) + ")")
        last_height = driver.execute_script("return document.body.scrollHeight")
        time.sleep(time_gap)
        y += 1
    log_message('Reached the bottom of page and reloaded all data')


def parallel_helper(func, arg_list, worker_count=8):
    """
    Behave exactly the same as map
    :param func:
    :param arg_list:
    :param worker_count:
    :return:
    """
    with closing(Pool(processes=worker_count)) as p:
        ret = p.map(func, arg_list)
        p.terminate()
    return ret
