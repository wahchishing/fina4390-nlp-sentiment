import datetime
import math
import sys
import os
from data_access.util_functions import *

path_to_file = os.path.dirname(os.path.realpath(__file__))
os.environ['PATH'] += os.pathsep + path_to_file + '/data_access'
import xueqiu


def parse_post(post):
	return_dict = {
		'title': post.title,
		'post': post.text,
		'like': post.like_count,
		'reply': post.reply_count,
		'retweet': post.retweet_count,
		'post_time': str(post.created_at.to('local').datetime)[:-6],
		'scrape_time': str(datetime.datetime.now())[:-3]
	}
	return return_dict


def get_xueqiu_post(ticker='00700', source='all', max_count=100, sort='time', end_date=None):
	"""
	:param ticker: stock code
	:param source: all, user讨论, news新闻, notice公告, trans交易
	:param max_count: max. number of posts returned
	:param sort: time最新, reply评论, relevance默认. 'time' recommended. relevance or reply may return posts created a few
	:param end_date: data before end_date will not be returned
	years ago
	:return dict
	"""
	s = xueqiu.Stock(ticker)
	n = math.ceil(max_count // 20)
	posts_list = []
	for i in range(n):
		if i < n - 1:
			s.get_posts(source=source, count=20, page=i + 1, sort=sort)
		else:
			s.get_posts(source=source, count=max_count - 20 * (i - 1), page=i + 1, sort=sort)
		posts_list.append(s.posts['list'])
		if end_date == None:
			continue
		elif parse_post(posts_list[-1][-1]).get('post_time') > end_date:
			break
	dict_list = [parse_post(p) for l in posts_list for p in l]
	log_message(f'XUEQIU: {ticker} data ready')
	return dict_list

