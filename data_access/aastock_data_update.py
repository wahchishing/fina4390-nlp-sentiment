import os
import pandas as pd
from data_access import aastock_crawling
import timeit
from data_access.util_functions import *

Universe = ['00001', '00002', '00003', '00005', '00006', '00011', '00012', '00016', '00017', '00019', '00027',
               '00066', '00083', '00101', '00151', '00175', '00267', '00270', '00288', '00291', '00384', '00386',
               '00388', '00656', '00669', '00688', '00700', '00728', '00762', '00788', '00823', '00857', '00883',
               '00914', '00939', '00941', '00960', '00966', '00998', '01038', '01044', '01088', '01093', '01099',
               '01109', '01113', '01177', '01193', '01211', '01288', '01299', '01336', '01339', '01398', '01658',
               '01766', '01800', '01918', '01928', '01988', '01997', '02007', '02018', '02020', '02202', '02313',
               '02318', '02319', '02328', '02382', '02388', '02601', '02628', '02688', '03328', '03968', '03988',
               '06030', '06837', 'HSI', 'HSCEI']

HSI_cons = ['00001', '00002', '00003', '00005', '00006', '00011', '00012', '00016', '00017', '00019', '00027', '00066',
            '00083', '00101', '00151', '00175', '00267', '00288', '00386', '00388', '00669', '00688', '00700', '00762',
            '00823', '00857', '00883', '00939', '00941', '01038', '01044', '01088', '01093', '00001', '01109', '01113',
            '01177', '01299', '01398', '01928', '01997', '02007', '02018', '02313', '02318', '02319', '02382', '02388',
            '02628', '03988']

HSCEI_cons = ['00151', '00175', '00267', '00270', '00291', '00384', '00386', '00656', '00700', '00728', '00788', '00857',
              '00883', '00914', '00939', '00941', '00960', '00966', '00998', '01044', '01088', '01093', '01099', '01109',
              '01177', '01193', '01211', '01288', '01336', '01339', '01398', '01658', '01766', '01800', '01918', '01988',
              '02007', '02020', '02202', '02313', '02318', '02328', '02601', '02628', '02688', '03328', '03968', '03988',
              '06030', '06837']

class Update_Data():
    def __init__(self,Dict):
        self.HSI_cons = ['00001', '00002', '00003', '00005', '00006', '00011', '00012', '00016', '00017', '00019', '00027', '00066',
            '00083', '00101', '00151', '00175', '00267', '00288', '00386', '00388', '00669', '00688', '00700', '00762',
            '00823', '00857', '00883', '00939', '00941', '01038', '01044', '01088', '01093', '00001', '01109', '01113',
            '01177', '01299', '01398', '01928', '01997', '02007', '02018', '02313', '02318', '02319', '02382', '02388',
            '02628', '03988']

        self.HSCEI_cons = ['00151', '00175', '00267', '00270', '00291', '00384', '00386', '00656', '00700', '00728', '00788', '00857',
              '00883', '00914', '00939', '00941', '00960', '00966', '00998', '01044', '01088', '01093', '01099', '01109',
              '01177', '01193', '01211', '01288', '01336', '01339', '01398', '01658', '01766', '01800', '01918', '01988',
              '02007', '02020', '02202', '02313', '02318', '02328', '02601', '02628', '02688', '03328', '03968', '03988',
              '06030', '06837']

        self.Source = Dict['Source']
        self.Dir = Dict['Dir']
        self.language = Dict['language']

    class aastock_data():
        def Update_Data(self,Ticker,language='en'):
            if Ticker == 'HSI':
                log_message('Start getting HSI content')
                ds_list = []
                whole_ds=pd.DataFrame()
                for cons in self.HSI_cons:
                    ds = pd.read_csv(self.Dir + r'aastock\\aastock_' + cons + '.csv')
                    ds_list.append(ds)
                for ds in ds_list:
                    ds.drop(columns=[ds.columns[0]], inplace=True)
                    whole_ds = pd.concat([whole_ds, ds], axis=0)
                # whole_ds.loc[:, 'news_time'] = pd.to_datetime(whole_ds['news_time'])
                whole_ds.sort_values(by=['news_time'], ascending=False, inplace=True)
                whole_ds.reset_index(inplace=True)
                whole_ds.drop(columns='index',inplace=True)
                whole_ds.to_csv(self.Dir + r'aastock\\aastock_' + Ticker + '.csv')
                log_message('AASTOCK: HSI data ready')

            elif Ticker == 'HSCEI':
                log_message('Start getting HSCEI content')
                ds_list = []
                whole_ds = pd.DataFrame()
                for cons in self.HSCEI_cons:
                    ds = pd.read_csv(self.Dir + r'aastock\\aastock_' + cons + '.csv')
                    ds_list.append(ds)
                for ds in ds_list:
                    ds.drop(columns=[ds.columns[0]], inplace=True)
                    whole_ds = pd.concat([whole_ds, ds], axis=0)
                # whole_ds.loc[:, 'news_time'] = pd.to_datetime(whole_ds['news_time'])
                whole_ds.sort_values(by=['news_time'], ascending=False, inplace=True)
                whole_ds.reset_index(inplace=True)
                whole_ds.drop(columns='index', inplace=True)
                whole_ds.to_csv(self.Dir + r'aastock\\aastock_' + Ticker + '.csv')
                log_message('AASTOCK: HSCEI data ready')

            else:
                old_ds = pd.read_csv(self.Dir + r'aastock\\aastock_' + Ticker + '.csv')
                old_ds.drop(columns=[old_ds.columns[0]], inplace=True)
                now = pd.to_datetime('today')
                last = pd.to_datetime(old_ds.loc[:,'news_time'][0])
                num_days = (now - last).days + 1
                # input_param_dict = {'ticker': Ticker, 'num_days': num_days, 'language': language, 'get_content': True,'parallel': False}
                input_param_dict = {'ticker': Ticker,'num_days': num_days,'language': language,'get_content': True,'parallel': True}
                new_dict1 = aastock_crawling.get_aastock_company(input_param_dict)
                new_ds1 = pd.DataFrame(new_dict1)
                whole_ds = pd.concat([new_ds1,old_ds], axis=0)
                whole_ds.loc[:,'stock_code'].fillna(method='ffill', inplace=True)
                whole_ds.drop_duplicates(subset=['news_id'], inplace=True)
                whole_ds.loc[:,'news_time']=pd.to_datetime(whole_ds['news_time'])
                whole_ds.sort_values(by=['news_time'], ascending=False, inplace=True)
                whole_ds.to_csv(self.Dir + r'aastock\\aastock_' + Ticker + '.csv')




if __name__== "__main__":
    sample_dict = {'Source'  : 'aastock',
                   'Dir'     : r'C:\\Users\\littl_000\\Desktop\\FINA4390\\data\\',  ##sample Dir, where you save the data
                   'language': 'en'}

    Update = Update_Data(Dict = sample_dict)
    Universe = ['00001', '00002', '00003', '00005', '00006', '00011', '00012', '00016', '00017', '00019', '00027',
                   '00066', '00083', '00101', '00151', '00175', '00267', '00270', '00288', '00291', '00384', '00386',
                   '00388', '00656', '00669', '00688', '00700', '00728', '00762', '00788', '00823', '00857', '00883',
                   '00914', '00939', '00941', '00960', '00966', '00998', '01038', '01044', '01088', '01093', '01099',
                   '01109', '01113', '01177', '01193', '01211', '01288', '01299', '01336', '01339', '01398', '01658',
                   '01766', '01800', '01918', '01928', '01988', '01997', '02007', '02018', '02020', '02202', '02313',
                   '02318', '02319', '02328', '02382', '02388', '02601', '02628', '02688', '03328', '03968', '03988',
                   '06030', '06837', 'HSI', 'HSCEI']

    start = timeit.default_timer()
    for Ticker in Universe:
        Update.aastock_data.Update_Data(self=Update,Ticker=Ticker)
    stop = timeit.default_timer()
    print('Time: ', stop - start)

