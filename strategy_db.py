from strategy_script import *
import numpy as np


def init_df(df):
    """
    use df['ret'] as the default return used by StrategyTester
    :param df:
    :return:
    """
    df.loc[:, 'ret'] = df.loc[:, 'HSI_close'].shift(-1) / df.loc[:, 'HSI_close'] - 1
    df = df.dropna()
    df = df.drop(columns=['HSI_close', 'HSI_open', 'HSI_high', 'HSI_low', 'HSI_volume', 'number_of_news'])
    df = df.loc[df['date_time'] > '2017', :]
    return df.dropna()


def modi_strat_1(df, norm_window, threshold):  # 1600 to 1600
    log_message('Running modi_strat_1 | norm_window: {} | threshold: {}'.format(norm_window, threshold))
    _df = df.copy(deep=True)
    ma = _df.loc[:, 'score_content'].rolling(window=norm_window).mean()
    sd = _df.loc[:, 'score_content'].rolling(window=norm_window).std()
    _df.loc[:, 'score_content'] = (df.loc[:, 'score_content'] - ma) / sd
    _df['signal'] = _df['score_content'].apply(lambda x: 1 if x > threshold else -1 if x < threshold else 0)
    return _df['signal']


modi_strat_1_params_dict = {
    'norm_window': list(range(1, 101)),
    'threshold': [round(i, 3) for i in list(np.linspace(0.01, 0.99, 99))]
}
modi_strat_1_param_dict = {
    'norm_window': 4,
    'threshold': 0.04
}
modi_strategy_1 = {
    'init_func': init_df,
    'strategy_name': 'modified strategy 1',
    'strategy_func': modi_strat_1,
    'params_dict': modi_strat_1_params_dict,
    'param_dict': modi_strat_1_param_dict
}


def init2_df(df):
    """
    use df['ret'] as the default return used by StrategyTester
    :param df:
    :return:
    """
    df.loc[:, 'ret'] = df.loc[:, 'HSI_close'] / df.loc[:, 'HSI_open'] - 1
    df = df.dropna()
    df = df.drop(columns=['HSI_close', 'HSI_open', 'HSI_high', 'HSI_low', 'HSI_volume', 'number_of_news'])
    df = df.loc[df['date_time'] > '2017', :]
    return df.dropna()


def modi2_strat_1(df, norm_window, threshold):  # 0920 to 0920
    log_message('Running modi_strat_1 | norm_window: {} | threshold: {}'.format(norm_window, threshold))
    _df = df.copy(deep=True)
    ma = _df.loc[:, 'score_content'].rolling(window=norm_window).mean()
    sd = _df.loc[:, 'score_content'].rolling(window=norm_window).std()
    _df.loc[:, 'score_content'] = (df.loc[:, 'score_content'] - ma) / sd
    _df['signal'] = _df['score_content'].apply(lambda x: 1 if x > threshold else -1 if x < threshold else 0)
    return _df['signal']


modi2_strat_1_params_dict = {
    'norm_window': list(range(1, 101)),
    'threshold': [round(i, 3) for i in list(np.linspace(0.01, 0.99, 99))]
}
modi2_strat_1_param_dict = {
    'norm_window': 4,
    'threshold': 0.04
}
modi2_strategy_1 = {
    'init_func': init2_df,
    'strategy_name': 'modified strategy 1',
    'strategy_func': modi2_strat_1,
    'params_dict': modi2_strat_1_params_dict,
    'param_dict': modi2_strat_1_param_dict
}

#
# def strat_1(df, long_threshold, short_threshold):
#     _df = df.copy(deep=True)
#     _df['signal'] = _df['score_content'].apply(lambda x: 1 if x > long_threshold else -1 if x < short_threshold else 0)
#     return _df['signal']
#
#
# strat_1_params_dict = {
#     'long_threshold': np.linspace(0, .2, 21),
#     'short_threshold': np.linspace(0, -.2, 21)
# }
# strat_1_param_dict = {
#     'long_threshold': 0.5,
#     'short_threshold': -0.5
# }
# strategy_1 = {
#     'init_func': init_df,
#     'strategy_name': 'strategy 1',
#     'strategy_func': strat_1,
#     'params_dict': strat_1_params_dict,
#     'param_dict': strat_1_param_dict
# }
#
#
# def strat_2(df, ma_window, threshold):
#     _df = df.copy(deep=True)
#     _df['ma'] = _df['score_content'].rolling(window=ma_window).mean()
#     _df.dropna(inplace=True)
#     _df['signal'] = _df.apply(
#         lambda x: 1 if (x['score_content'] > x['ma']) & (x['score_content'] > threshold) else
#         -1 if (x['score_content'] < x['ma']) & (x['score_content'] < -threshold) else 0, axis=1)
#     return _df['signal']
#
#
# strat_2_params_dict = {
#     'ma_window': [3, 5, 7, 10, 14, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150],
#     'threshold': np.linspace(0, .2, 21)
# }
# strat_2_param_dict = {
#     'ma_window': 20,
#     'threshold': 0.08
# }
# strategy_2 = {
#     'init_func': init_df,
#     'strategy_name': 'strategy 2',
#     'strategy_func': strat_2,
#     'params_dict': strat_2_params_dict,
#     'param_dict': strat_2_param_dict
# }
#
#
# def strat_3(df, short_window, long_window):
#     _df = df.copy(deep=True)
#     if short_window >= long_window:
#         _df['signal'] = _df['ret'] * 0
#         return _df['signal'], _df['ret']
#     _df['long_ma'] = _df['score_content'].rolling(window=long_window).mean()
#     _df['short_ma'] = _df['score_content'].rolling(window=short_window).mean()
#     _df.dropna(inplace=True)
#     _df['signal'] = _df.apply(
#         lambda x: 1 if x['short_ma'] > x['long_ma'] else -1 if x['long_ma'] > x['short_ma'] else 0, axis=1)
#     return _df['signal']
#
#
# strat_3_params_dict = {
#     'short_window': list(range(3, 25, 2)),
#     'long_window': [5, 7, 10, 14, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 252, 300]
# }
# strat_3_param_dict = {
#     'short_window': 14,
#     'long_window': 50
# }
# strategy_3 = {
#     'init_func': init_df,
#     'strategy_name': 'strategy 3',
#     'strategy_func': strat_3,
#     'params_dict': strat_3_params_dict,
#     'param_dict': strat_3_param_dict
# }
#
#
# def strat_4(df, short_window, long_window):
#     #log_message('Running Strategy 1 | short_window = {} | long_window = {}'.format(short_window, long_window))
#     _df = df.copy(deep=True)
#     if short_window >= long_window:
#         _df['signal'] = _df['ret'] * 0
#         return _df['signal'], _df['ret']
#     _df['long_ma'] = _df['score_content'].rolling(window=long_window).mean()
#     _df['short_ma'] = _df['score_content'].rolling(window=short_window).mean()
#     _df.dropna(inplace=True)
#     _df['signal'] = _df.apply(
#         lambda x: 1 if x['short_ma'] > x['long_ma'] else -1 if x['long_ma'] > x['short_ma'] else 0, axis=1)
#     for i in range(1, _df.shape[0]):
#         if _df['signal'].iloc[i - 1] != 0:
#             _df['signal'].iloc[i] = 0
#     return _df['signal']
#
#
# strat_4_params_dict = {
#     'short_window': [3, 5, 7, 10, 14, 20, 30, 40, 50],
#     'long_window': [5, 7, 10, 14, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 252, 300]
# }
# strat_4_param_dict = {
#     'short_window': 14,
#     'long_window': 50
# }
# strategy_4 = {
#     'init_func': init_df,
#     'strategy_name': 'strategy 4',
#     'strategy_func': strat_4,
#     'params_dict': strat_4_params_dict,
#     'param_dict': strat_4_param_dict
# }
