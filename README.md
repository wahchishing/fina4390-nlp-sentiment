# FINA4390 NLP Sentiment
## Anthony | Matt | Vincent | Ernest

---
## Meeting Minutes

### 7/2 1st Meeting:
* Group news by: 1) news source; 2) news category
* News category:
    * Official announcement;
    * Official news sources
    * Social media
* Digest all the news and then trade on the next day
* Try to analyze how much info have been incorporated in the price
* Target index: HSI & HSCEI
* More detailed weekly timeline

### 14/2 2nd Meeting:
* Retrieve news from Reuters, Bloomberg, social media (twitter...)
* Use NLP library to construct sentiment scores

### 21/2 3rd Meeting:
* Statistical analysis on retrieved data
* Feature engineering
* Backtest on text data

### 28/2 4th Meeting:
* Adjust construction of sentiment score based on back test result

### 6/3 5th Meeting:
### 13/3 6th Meeting:
### 20/3 7th Meeting:
### 27/3 8th Meeting:
### 3/4 9th Meeting:
### 10/4 10th Meeting:
### 17/4 11th Meeting:
### 24/4 12th Meeting:
* Presentation






---



## Tentative Meeting Schedule

### Week 1 (7/2)
- Review research materials (literature, papers)
- Review investment mandate
- Review data source, data collection method
### *Week 2 (14/2)*
- Report progress

### Week 3 (21/2)
- Report progress

### Week 4 (28/2)
- Review sentiment analysis methodology
- Sample testing on random text data

### Week 5 (6/3)
- Review Sample testing

### Week 6 (13/3)
- Review data source quality
- Potentially add more data source
- Draft trading signal based on sentiment data

### Week 7 (20/3)
- Report progress

### Week 8 (27/3)
- Review strategy: buy/sell signal
- Review sentiment data quality

### Week 9 (3/4)
- Report progress

### Week 10 (10/4)
- Report progress

### Week 11 (17/4)
- Review backtest result
- Review parameter optimizing algorithm

### Week 12 (24/4) *LAST MEETING*
- Finalise project result

