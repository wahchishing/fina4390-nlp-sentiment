from data_access.aastock_crawling import *
from data_access.util_functions import *
from sentiment_score_calculation.vader_sentiment import *
import datetime
import itertools

import pandas as pd
df = pd.read_csv('data/bloomberg/HSI_weighting.csv',index_col=0)
hsi_components = df.columns
HSCEI_cons = ['00151', '00175', '00267', '00270', '00291', '00384', '00386', '00656', '00700', '00728', '00788',
              '00857',
              '00883', '00914', '00939', '00941', '00960', '00966', '00998', '01044', '01088', '01093', '01099',
              '01109',
              '01177', '01193', '01211', '01288', '01336', '01339', '01398', '01658', '01766', '01800', '01918',
              '01988',
              '02007', '02020', '02202', '02313', '02318', '02328', '02601', '02628', '02688', '03328', '03968',
              '03988',
              '06030', '06837']
stock_universe = list(set(hsi_components) | set(HSCEI_cons))

def get_hsi_components_news(stock_list=None, from_date='2020-01-15', language='en', get_score=True):
    if stock_list is None:
        stock_list = hsi_components
    log_message('Start getting all news')
    days = (datetime.datetime.today().date() -
            datetime.datetime.strptime(from_date, '%Y-%m-%d').date()).days
    input_param_dict_list = [{
        'ticker': stock_code,
        'num_days': days,
        'language': language,
        'get_content': True,
        'parallel': True
    } for stock_code in stock_list]
    result_list = list(map(get_aastock_company, input_param_dict_list))
    full_list = itertools.chain.from_iterable(result_list)
    df = pd.DataFrame(full_list)
    df.loc[:, 'news_date'] = [str(x)[:10] for x in df.loc[:, 'news_time'].tolist()]
    df = df.loc[df['news_date'] > from_date, :]
    if get_score:
        score_helper = SentimentScoreHelper()
        df.loc[:, 'vader_score'] = [score_helper.get_sentiment_score(
            text, 'compoound') for text in df.loc[:, 'news_content'].tolist()]
    return df

if __name__ == '__main__':
    df = get_hsi_components_news(stock_list=stock_universe,from_date='2011-01-01',get_score=False)
    df.to_csv('xdd.csv', index=False)
    