import pandas as pd
import datetime
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import os
from contextlib import closing
from multiprocessing import Pool
from matplotlib import style
from strategy_db import *
style.use('seaborn')

# Define the global variables to be used here

PROJECT_DIR = os.path.abspath(__file__)[:os.path.abspath(__file__).find('nlp-sentiment') + 13]
CSV_NAME = 'vader_HSI_score_1600_1600.csv'
CSV_NAME_LIST = list(filter(lambda x: 'aastock' not in x and 'expanded' not in x and 'bloomberg' not in x and 'full' not in x, os.listdir(os.path.join(PROJECT_DIR, 'data', 'aastock'))))
HEATMAP_V = {
    'Sharpe Ratio': (-1.5, 2),
    'Winning Rate': (0.35, 0.65),
    'Annual Return': (-0.2, 0.2),
    'Maximum Drawdown': (-0.4, 0.0),
    'sortino': (-0.8, 1.8),
    'Calmar Ratio': (-0.5, 2)
}
METRICS_INPUT = ['Annual Return', 'Maximum Drawdown', 'Sharpe Ratio', 'sortino', 'Calmar Ratio', 'Winning Rate']


class StrategyTester:
    def __init__(self, strategy_dict, csv_name=CSV_NAME):
        log_message('Target_df: {}'.format(csv_name))
        self.data_set_name = csv_name.split('.')[0]
        self.csv_path = os.path.join(PROJECT_DIR, 'data/aastock/', csv_name)
        self.df = strategy_dict['init_func'](pd.read_csv(self.csv_path))
        self.ret = self.df['ret']
        self.metrics = METRICS_INPUT
        self.strategy_function = strategy_dict['strategy_func']
        self.output_dir = os.path.join(PROJECT_DIR, 'output_results')
        if not os.path.exists(self.output_dir):
            os.mkdir(self.output_dir)
        self.strategy_name = strategy_dict['strategy_name']
        self.params_dict = strategy_dict['params_dict']

    def plot_strategy_metrics(self, save=True):
        input_params_dict = self.params_dict
        log_message('Start plotting strategy metrics of {}'.format(self.strategy_name))
        [p1_name, p1_list], [p2_name, p2_list] = input_params_dict.items()
        input_params_list = [[self.strategy_function, {'df': self.df, p1_name: p1, p2_name: p2}, self.ret] for p1 in p1_list for p2 in p2_list]
        result = list(zip([i[1] for i in input_params_list], parallel_helper(get_result_metrics, input_params_list)))
        result_dict = {m: {p2: [get_one_result(result, p1_name, p2_name, p1, p2, m) for p1 in p1_list]
                           for p2 in p2_list} for m in self.metrics}
        metrics_df_dict = {m: get_one_df(result_dict, m, p1_list, p2_list) for m in self.metrics}
        log_message('Test on strategy {} finished'.format(self.strategy_function.__name__))
        num_row = int(np.ceil(np.sqrt(len(self.metrics))))
        num_col = int(len(self.metrics) // num_row)
        if num_row * num_col < len(self.metrics):
            num_col += 1
        cnt = 1
        fig = plt.figure(figsize=(70 * num_row, 56 * num_col))
        for m in self.metrics:
            fig.add_subplot(num_row, num_col, cnt)
            cnt += 1
            vmin, vmax = HEATMAP_V.get(m, (None, None))
            sns.heatmap(metrics_df_dict[m], vmin=vmin, vmax=vmax, annot=True, fmt='.2f', cmap='RdYlGn')
            plt.xlabel(p2_name)
            plt.ylabel(p1_name)
            plt.title(m)
        fig.tight_layout()
        if save:
            save_name = os.path.join(self.output_dir, self.strategy_name.replace(' ', '_') + '_' + self.data_set_name + 'metrics.png')
            fig.savefig(save_name)
            log_message('Result graph saved as {}'.format(save_name))
        else:
            plt.show()
        return metrics_df_dict

    def test_strategy(self, input_param_dict, save=True):
        param_dict = input_param_dict.copy()
        input_param_dict['df'] = self.df
        ret = self.ret
        signal = self.strategy_function(**input_param_dict)
        ret, signal = map_ret_signal(ret, signal)
        pnl = signal * ret
        pnl = pnl[signal != 0].reset_index(drop=True)
        ret = ret[signal != 0].reset_index(drop=True)
        if save:
            style.use('seaborn')
            log_message('Start plotting strategy backtest results of {}'.format(self.strategy_name))
            beta = np.cov(pnl, ret)[0, 1] / ret.var()
            alpha = pnl - beta * ret

            # Cumulative alpha plot
            plt.figure(dpi=150, figsize=(21, 12))
            plt.subplot(1, 2, 1)
            m = round(alpha.mean() * 252, 4)
            beta = round(beta, 4)
            plt.plot(alpha.cumsum(), label='alpha')
            plt.legend()
            plt.xlabel('number of trades')
            plt.ylabel('cummulative alpha')
            plt.title('{} | annual alpha = {} | beta = {}'.format(self.strategy_function.__name__, m, beta))

            # Alpha per trade plot
            plt.subplot(1, 2, 2)
            plt.hist(alpha, label='alpha', bins=50)
            plt.legend()
            plt.xlabel('alpha per trade')
            plt.title('{} | annual alpha = {} | beta = {}'.format(self.strategy_function.__name__, m, beta))
            plt.tight_layout()
            name_constructor = '_'.join(['{}_{}'.format(i.replace(' ', '_'), str(j)) for i, j in param_dict.items()])
            save_name = os.path.join(
                self.output_dir, self.strategy_name.replace(' ', '_') + '_backtest_' + name_constructor + '_' + self.data_set_name + '.png')
            plt.savefig(save_name)
            log_message('Result graph saved as {}'.format(save_name))
        input_param_dict['df'] = self.df
        input_param = [self.strategy_function, input_param_dict, self.ret]
        return get_result_metrics(input_param)


def get_one_result(input_list, p1n, p2n, p1v, p2v, mn):
    return list(filter(lambda x: x[0][p1n] == p1v and x[0][p2n] == p2v, input_list))[0][1][mn]


def get_one_df(input_dict, mn, p1l, p2l):
    df = pd.DataFrame(input_dict[mn])
    df = df.loc[:, p2l]
    df.index = p1l
    return df.astype('float64')


def map_ret_signal(input_ret, input_signal):
    tmp = pd.DataFrame({'ret': input_ret, 'signal': input_signal})
    tmp.dropna(inplace=True)
    return tmp['ret'], tmp['signal']


def get_result_metrics(input_params):
    strategy_function = input_params[0]
    param_dict = input_params[1]
    hsi_ret = input_params[2]
    signal = strategy_function(**param_dict)
    hsi_ret, signal = map_ret_signal(hsi_ret, signal)
    return_dict = {i: 0 for i in METRICS_INPUT}
    if not signal.any():
        return return_dict
    pnl = signal * hsi_ret
    cumret = pnl.cumsum() - 1
    drawdown = cumret - cumret.cummax()
    mdd = drawdown.min()
    annual_ret = pnl.mean() * 252
    annual_vol = pnl.std() * np.sqrt(252)
    sharpe = annual_ret / annual_vol
    calmar = annual_ret / abs(mdd)
    return {
        'Maximum Drawdown': mdd,
        'Annual Return': annual_ret,
        'Annual Volatility': annual_vol,
        'Winning Rate': len(pnl[pnl > 0]) / (len(pnl[pnl > 0]) + len(pnl[pnl < 0])),
        'Sharpe Ratio': sharpe,
        'sortino': pnl.mean() / pnl[pnl < 0].std() * np.sqrt(252),
        'Calmar Ratio': calmar
    }


def log_message(message, err_message=None):
    if not err_message:
        log_str = '{}: {}'.format(str(datetime.datetime.today())[:-3], message)
    else:
        log_str = '{}: <<<ERROR:{}>>> {}'.format(str(datetime.datetime.today())[:-3], err_message, message)
    print(log_str)
    return None


def parallel_helper(func, arg_list, worker_count=8):
    """
    Behave exactly the same as map
    :param func:
    :param arg_list:
    :param worker_count:
    :return:
    """
    with closing(Pool(processes=worker_count)) as p:
        ret = p.map(func, arg_list)
        p.terminate()
    return ret

