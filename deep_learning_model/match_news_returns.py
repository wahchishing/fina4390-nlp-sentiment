import pandas as pd
from data_access.get_stock_price_yahoo import get_hk_stock_price
import datetime
import numpy as np


def get_news_date(news_time):
    if news_time.time() <= datetime.time(16):
        news_date = news_time.date()
    else:
        news_date = news_time.date() + datetime.timedelta(1)
    return str(news_date)

def match_news_returns(df):
    df.sort_values(by='news_time', inplace=True)
    df.news_time = pd.to_datetime(df.news_time)
    df['news_date'] = [get_news_date(nt) for nt in df.news_time]
    df['stock_code'] = df['stock_code'].apply(lambda sc: format(sc, '04') + ".HK")
    stock_code = set(df.stock_code)
    for sc in stock_code:
        try:
            tmp = df[df['stock_code'] == sc]['news_time']
            end_date = str(tmp.iloc[-1].date() + datetime.timedelta(7))
            start_date = str(tmp.iloc[0].date() -datetime.timedelta(7))

            price = get_hk_stock_price(sc, start_date, end_date)
            price['ret0'] = price['adjclose'].shift(-1)/price['adjclose'] - 1
            price['ret1'] = price['adjclose'].shift(-1)/price['adjclose'].shift() - 1
            # dynamic threshold price (may have bug, recommend change ret1 only as ret0 will be used to cal returns)
            price['vol'] = price['ret0'].rolling(14).std().shift()
            price['ret1'] = price['ret1']/price['vol']

            ret_dict = {price.loc[ind,'date_time']: (price.loc[ind,'ret0'], price.loc[ind, 'ret1']) for ind in price.index}
            ind = df.stock_code == sc
            tmp = np.array([ret_dict.get(date ,(0,0)) for date in df.loc[ind,'news_date']])
            df.loc[ind,'ret0'], df.loc[ind,'ret1'] = tmp[:,0], tmp[:,1]
        except Exception as e:
            print(sc)

if __name__=='__main__':
    df = pd.read_csv('data/aastock/expanded_aastock_HSI.csv')
    match_news_returns(df)
    # df.to_csv('deep_learning_model/data/aastock_expaned_aastock_HSI_ret.csv', index=False)
