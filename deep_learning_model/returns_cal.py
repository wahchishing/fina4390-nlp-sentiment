import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import style
import seaborn as sns
import os
style.use('ggplot')

PATH_TO_FILE = os.path.dirname(os.path.realpath(__file__))

DATA_PATH = os.path.join(PATH_TO_FILE, 'data/BERT')

# cal IS, OS, overall sharpe
def get_sharpe(lb, ub, in_df, out_df, over_df):
    def transform(x):
        if x < lb:
            return -1
        elif x > ub:
            return 1
        else:
            return 0

    df['size'] = df['score'].apply(transform)
    df['pnl'] = df['ret0'] * df['size']
    date_list = sorted(set(df['news_date']))
    tmp = df[df['size'] != 0]
    ############ add transaction fee
    # tmp['pnl'] -= 0.002
    ret_df = tmp.groupby('news_date').mean()['pnl']
    # include missing date/ need optimize performance
    try:
        d_list = list(ret_df.index)
        ind_list = [str(i.date()) for i in pd.bdate_range(d_list[0], d_list[-1]).to_list()]
        filter_list = filter(lambda x: x not in ind_list, date_list)
        # for date in date_list:
        #     if date not in filter_list:
        #         ret_df[date] = 0
        ret_df.loc[filter_list] = 0
    except IndexError:
        pass

    ret_df = ret_df.reset_index()
    ins = ret_df[ret_df['news_date'] < '2019']
    outs = ret_df[ret_df['news_date'] >= '2019']
    s1 = (ins.mean()/ins.std() * 252**0.5).iloc[0]
    s2 = (outs.mean()/outs.std() * 252**0.5).iloc[0]
    s3 = (ret_df['pnl'].mean()/ret_df['pnl'].std() * 252**0.5)
    in_df.loc[lb,ub] = s1
    out_df.loc[lb,ub] = s2
    over_df.loc[lb,ub] = s3


dir_list = os.listdir(DATA_PATH)
for dir in dir_list:
    df = pd.read_csv(f'{DATA_PATH}/{dir}')
    threshold = float(dir.split('_')[1].split('.csv')[0])

    ub = list(map(lambda x: round(x,2), np.linspace(0.5,0.8,7)))
    lb = list(map(lambda x: round(x,2), np.linspace(0.5,0.2,7)))
    ins = pd.DataFrame(index=lb, columns=ub)
    outs = pd.DataFrame(index=lb, columns=ub)
    overs = pd.DataFrame(index=lb, columns=ub)
    params = [(l, u ,ins,outs,overs) for l in lb for u in ub]

    for param in params:
        get_sharpe(*param)

    plt.figure(dpi=200, figsize=(8,6))
    sns.heatmap(ins.astype('float64'), vmin= 0 , vmax=5, annot=True, fmt='.2f', cmap='RdYlGn')
    plt.ylabel('Short Threshold')
    plt.xlabel('Long Threshold')
    plt.title(f'In-Sample ({threshold:.3f})')
    plt.savefig(f'{os.path.dirname(os.path.dirname(DATA_PATH))}/plot/3988_IS_{threshold:.3f}.png')

    plt.figure(dpi=200, figsize=(8,6))
    sns.heatmap(outs.astype('float64'), vmin= -2 , vmax=3, annot=True, fmt='.2f', cmap='RdYlGn')
    plt.ylabel('Short Threshold')
    plt.xlabel('Long Threshold')
    plt.title(f'Out-of-Sample ({threshold:.3f})')
    plt.savefig(f'{os.path.dirname(os.path.dirname(DATA_PATH))}/plot/3988_OS_{threshold:.3f}.png')

    plt.figure(dpi=200, figsize=(8,6))
    sns.heatmap(overs.astype('float64'), vmin= -1 , vmax=4, annot=True, fmt='.2f', cmap='RdYlGn')
    plt.ylabel('Short Threshold')
    plt.xlabel('Long Threshold')
    plt.title(f'Overall ({threshold:.3f})')
    plt.savefig(f'{os.path.dirname(os.path.dirname(DATA_PATH))}/plot/3988_Overall_{threshold:.3f}.png')




# def transform(x):
#     if x < lb:
#         return -1
#     elif x > ub:
#         return 1
#     else:
#         return 0
# dir_list
# df = pd.read_csv('deep_learning_model/data/hkex/hkex_ 0.020.csv')
# plt.hist(df.score,bins=100)
# lb, ub = 0.5,0.5
# def transform(x):
#     if x < lb:
#         return -1
#     elif x > ub:
#         return 1
#     else:
#         return 0
#
# df['size'] = df['score'].apply(transform)
# df['pnl'] = df['ret0'] * df['size']
# date_list = sorted(set(df['news_date']))
# tmp = df[df['size'] != 0]
# ret_df = tmp.groupby('news_date').mean()['pnl']
# for date in date_list:
#     if date not in ret_df.index:
#         ret_df[date] = 0
# ret_df = ret_df.reset_index()
#
# ins = ret_df[ret_df['news_date'] < '2019']
# outs = ret_df[ret_df['news_date'] >= '2019']
#
#
#
# plt.figure(dpi=200, figsize=(10,6))
# plt.plot(pd.to_datetime(ins['news_date']),
#          ins['pnl'].cumsum(), label='In-Sample')
# s = ins['pnl'].sum()
# plt.plot(pd.to_datetime(outs['news_date']), s +
#          outs['pnl'].cumsum(), label='Out-of-Sample')
# plt.legend()
# plt.title(f'IS = {s1:.3g} | OS = {s2:.3g} | Overall = {s3:.3g}')
# plt.savefig('plot/BERT_overall.png')
#
# plt.figure(dpi=200, figsize=(10,6))
#
# ar = outs['pnl'].mean() * 252
# s2 = (outs.mean()/outs.std() * 252**0.5).iloc[0]
# mdd = min((1+outs['pnl'].cumsum()) / (1+outs['pnl'].cumsum()).cummax()-1)
# wr = outs[outs['pnl']>0].shape[0]/(outs[outs['pnl']>0].shape[0]+outs[outs['pnl']<0].shape[0])
# vol = outs['pnl'].std() * 252**0.5
# plt.plot(pd.to_datetime(outs['news_date']),
#          outs['pnl'].cumsum(), label='Out-of-Sample')
# plt.xticks(rotation=30)
# plt.title(
#     f'AR = {ar:.2%} | WR = {wr:.2%} | Sharpe = {s2:.3g} | MDD = {abs(mdd):.2%}')
