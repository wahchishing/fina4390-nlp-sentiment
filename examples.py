from data_access.get_stock_price_yahoo import *
from data_access.aastock_crawling import *

if __name__ == '__main__':
    stock_code = '0700.HK'
    # Get stock price

    df = get_hk_stock_price(stock_code=stock_code,
                            start_date='2017-01-01',
                            end_date='2019-01-01',
                            freq='daily')

    # Fetch popular news

    news_dict = get_aastock_news('latest-news', 'tc')



