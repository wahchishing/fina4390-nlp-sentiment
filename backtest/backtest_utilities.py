import logging
import datetime
import pandas as pd
from scipy.stats import linregress


def fetch_quantity_adjusted_with_beta(mkt_index_ts: pd.Series, individual_ts: pd.Series,
                                      reference_date: str, rollback_days=750):
    """
    :param mkt_index_ts: the close of market index time series in terms of pd.Series
    :param individual_ts: the close of individual stock time series in terms of pd.Series
    :param reference_date: in the format of '%Y-%m-%d', e.g.
    :param rollback_days: the parameter for calculating the beta based on the look-backward period,
                        default: 750 trading days/ ~3 years
    :return beta: float, df_after_reference_date: pd.Dataframe.
    :return None, if there are errors.
    """
    df = pd.concat([mkt_index_ts, individual_ts, mkt_index_ts.pct_change(), individual_ts.pct_change()], axis=1)
    df.columns = ['Market Index Close', 'Individual Close', 'Market Index %Change', 'Individual %Change']
    df = df.dropna(axis=0)
    if datetime.datetime.strptime(reference_date, '%Y-%m-%d') < df.index[0]:
        logging.debug(f'The reference date entered is too early so that it is earlier than the date of the first data.')
        return None

    df_before_reference_date = df[df.index <= reference_date]
    if df_before_reference_date.__len__() < rollback_days:
        logging.debug(f'The length of data is not long enough '
                      f'so that the rollback days for calculating Beta is larger than the data we have.')
        return None
    df_rollback = df_before_reference_date[-rollback_days:]
    slope, intercept, r_value, p_value, std_err = \
        linregress(df_rollback['Market Index %Change'], df_rollback['Individual %Change'])
    beta = slope
    df_after_reference_date = df[df.index > reference_date].copy()
    df_after_reference_date['Q_stock per Q_index'] = df_after_reference_date['Market Index Close']/(beta * df_after_reference_date['Individual Close'])
    return beta, df_after_reference_date
