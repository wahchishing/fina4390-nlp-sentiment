import numpy as np
from data_access.get_stock_price_yahoo import *
from matplotlib import pyplot as plt
from matplotlib import ticker as ticker
import matplotlib.dates as m_dates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


class BacktestingHelper:
    def __init__(self,
                 stock_code,
                 start_date,
                 end_date,
                 preprocess_func,
                 strategy_func,
                 initial=1000000,
                 bet_size_type='ALL_IN',
                 exit_size_type='ALL_OUT',
                 entry_trade_timing='at_close',
                 exit_trade_timing='at_close',
                 cost_factor=0.0015,
                 slippage_mean=0.001,
                 interest=0.0):
        self.stock_code = stock_code.zfill(7)
        self.start_date = start_date
        self.end_date = end_date
        self.stock_price_data = get_hk_stock_price(stock_code, start_date, end_date)
        self.processed_data = preprocess_func(self.stock_price_data)
        self.strategy = strategy_func
        self.initial_capital = initial
        self.entry_amount_type = bet_size_type
        self.exit_amount_type = exit_size_type
        self.trade_timing = entry_trade_timing
        self.exit_timing = exit_trade_timing
        self.trading_cost = cost_factor
        self.mean_slippage = slippage_mean
        self.interest_rate = interest
        self.backtest_df = self.__initialise_backtest_df()
        self.backtest_finished = False
        self.temp_entry_price = 0.0
        self.trade_records = {}
        self.result_statistic = {}

    def run_backtest(self, test_bar_size=1):
        if self.backtest_finished:
            return self.backtest_df
        for i in self.backtest_df.index:
            if i < test_bar_size - 1:
                self.__update_position(i)
                continue
            testing_bar = self.backtest_df.loc[i-test_bar_size+1:i, :]
            signal = self.strategy(testing_bar)
            if signal is None:
                self.__update_position(i)
                continue
            else:
                if signal == 'LONG':
                    self.__long_signal(i)
                elif signal == 'SHORT':
                    self.__short_signal(i)
                elif signal == 'EXIT_LONG':
                    self.__exit_long_signal(i)
                elif signal == 'EXIT_SHORT':
                    self.__exit_short_signal(i)
        self.backtest_finished = True
        self.backtest_df.loc[:, 'daily_return'] = self.backtest_df.loc[:, 'total_capital'].pct_change()
        self.result_statistic = self.get_result_stat()
        return self.backtest_df

    def get_result_stat(self):
        if not self.backtest_finished:
            print("Please run the backtest first.")
            return None
        elif bool(self.result_statistic):
            print_dict(self.result_statistic)
            return self.result_statistic
        else:
            return_dict = {
                'cumulative_return': self.__get_cumulative_return(),
                'annualised_return': self.__get_annualised_return(),
                'daily_volatility': self.__get_daily_volatility(),
                'annualised_volatility': self.get_annualised_volatility(),
                'maximum_drawdown': self.__get_mdd(),
                'annualised_sharpe_ratio': self.__get_annualised_sharpe(),
                'sortino_ratio': self.__get_sortino(),
                'calmer_ratio': self.__get_calmer()
            }
            print_dict(return_dict)
            return return_dict

    def get_trade_records(self):
        return_dict = {}
        entry_dict = {}
        count = 1
        for i in self.backtest_df.index:
            if not bool(entry_dict):
                if self.backtest_df.loc[i, 'entry_signal'] is not None:
                    entry_dict = self.backtest_df.loc[i, :].to_dict()
                    continue
            else:
                if self.backtest_df.loc[i, 'exit_signal'] is not None:
                    exit_dict = self.backtest_df.loc[i, :].to_dict()
                    combined_dict = {
                        'entry_date': entry_dict['date_time'],
                        'exit_date': exit_dict['date_time'],
                        'entry_price': entry_dict['executed_price'],
                        'exit_price': exit_dict['executed_price'],
                        'entry_type': entry_dict['entry_signal'],
                        'entry_amount_type': entry_dict['entry_amount_type'],
                        'exit_amount_type': exit_dict['exit_amount_type'],
                        'number_of_shares': entry_dict['position_shares'],
                        'single_trade_return': exit_dict['return_of_single_trade'],
                        'entry_cost_slippage': entry_dict['cost_and_slippage'],
                        'exit_cost_slippage': exit_dict['cost_and_slippage'],
                        'entry_total_capital': entry_dict['total_capital'],
                        'exit_total_capital': exit_dict['total_capital']
                    }
                    return_dict[count] = combined_dict
                    count += 1
                    entry_dict = {}
        self.trade_records = return_dict
        return return_dict

    def plot_graph(self, save_fig=True):
        register_matplotlib_converters()
        if not self.backtest_finished:
            print("Please run the backtest first.")
            return None
        x_dates = self.backtest_df.date_time.tolist()
        x_dates = [datetime.datetime.strptime(d, '%Y-%m-%d').date() for d in x_dates]
        stock_price_list = self.backtest_df.close.tolist()
        capital_list = self.backtest_df.total_capital.tolist()
        fig, ax1 = plt.subplots()
        ax1.xaxis.set_major_formatter(m_dates.DateFormatter('%Y-%m-%d'))
        ax1.xaxis.set_major_locator(m_dates.DayLocator())
        fig.autofmt_xdate()
        color = 'tab:blue'
        ax1.set_xlabel('Dates')
        ax1.set_ylabel(self.stock_code)
        line_one = ax1.plot(x_dates, stock_price_list, color=color, label=self.stock_code)
        ax1.tick_params(axis='y')
        ax2 = ax1.twinx()
        color = 'tab:green'
        ax2.set_ylabel(self.strategy.__name__ + '_capital')
        line_two = ax2.plot(x_dates, capital_list, color=color, label=self.strategy.__name__)
        ax2.tick_params(axis='y')
        lines = line_one + line_two
        labels = [line.get_label() for line in lines]
        ax1.legend(lines, labels, loc=0)
        plot_title = str(datetime.datetime.today().date()) + '_' + self.strategy.__name__
        plt.title(plot_title)
        ax1.tick_params(axis='x', rotation=30, labelsize=10)
        fig.tight_layout()
        plt.show()
        if save_fig:
            plt.savefig(plot_title + '.png')
        return None

    # Private functions
    
    def __initialise_backtest_df(self):
        result_df = self.processed_data.copy()
        result_df.loc[:, 'current_position'] = None
        result_df.loc[:, 'entry_signal'] = None
        result_df.loc[:, 'exit_signal'] = None
        result_df.loc[:, 'entry_amount_type'] = self.entry_amount_type
        result_df.loc[:, 'exit_amount_type'] = self.exit_amount_type
        result_df.loc[:, 'change_in_shares'] = 0.0
        result_df.loc[:, 'executed_price'] = 0.0
        result_df.loc[:, 'cost_and_slippage'] = 0.0
        result_df.loc[:, 'cash'] = self.initial_capital
        result_df.loc[:, 'position_shares'] = 0.0
        result_df.loc[:, 'position_value'] = 0.0
        result_df.loc[:, 'total_capital'] = self.initial_capital
        result_df.loc[:, 'total_pnl'] = 0.0
        result_df.loc[:, 'daily_return'] = 0.0
        result_df.loc[:, 'drawdown'] = 0.0
        result_df.loc[:, 'return_of_single_trade'] = 0.0
        return result_df
    
    def __long_signal(self, cur_loc):
        self.backtest_df.loc[cur_loc:, 'current_position'] = 'LONG'
        self.backtest_df.loc[cur_loc, 'entry_signal'] = 'LONG'
        self.backtest_df.loc[cur_loc, 'executed_price'] = self.__get_entry_executed_price(cur_loc)
        self.backtest_df.loc[cur_loc, 'change_in_shares'] = self.__get_entry_shares_calc(cur_loc, 'LONG')
        self.backtest_df.loc[cur_loc:, 'position_shares'] += self.backtest_df.loc[cur_loc, 'change_in_shares']
        self.backtest_df.loc[cur_loc, 'position_value'] = self.__get_position_value(cur_loc)
        self.backtest_df.loc[cur_loc, 'cost_and_slippage'] = self.__get_slippage_trading_cost(cur_loc)
        self.backtest_df.loc[cur_loc:, 'cash'] += self.__get_chg_in_cash(cur_loc)
        self.backtest_df.loc[cur_loc:, 'cash'] = self.__get_interest_cash(cur_loc)
        self.backtest_df.loc[cur_loc, 'total_capital'] = self.__get_period_total_capital(cur_loc)
        self.backtest_df.loc[cur_loc, 'total_pnl'] = self.__get_period_pnl(cur_loc)
        self.backtest_df.loc[cur_loc, 'drawdown'] = self.__get_period_mdd(cur_loc)
        self.temp_entry_price = self.backtest_df.loc[cur_loc, 'executed_price']
        self.backtest_df.loc[cur_loc, 'return_of_single_trade'] = self.__get_cur_single_abs_return(cur_loc)
        return None

    def __short_signal(self, cur_loc):
        self.backtest_df.loc[cur_loc:, 'current_position'] = 'SHORT'
        self.backtest_df.loc[cur_loc, 'entry_signal'] = 'SHORT'
        self.backtest_df.loc[cur_loc, 'executed_price'] = self.__get_entry_executed_price(cur_loc)
        self.backtest_df.loc[cur_loc, 'change_in_shares'] = self.__get_entry_shares_calc(cur_loc, 'SHORT')
        self.backtest_df.loc[cur_loc:, 'position_shares'] += self.backtest_df.loc[cur_loc, 'change_in_shares']
        self.backtest_df.loc[cur_loc, 'position_value'] = self.__get_position_value(cur_loc)
        self.backtest_df.loc[cur_loc, 'cost_and_slippage'] = self.__get_slippage_trading_cost(cur_loc)
        self.backtest_df.loc[cur_loc:, 'cash'] += self.__get_chg_in_cash(cur_loc)
        self.backtest_df.loc[cur_loc:, 'cash'] = self.__get_interest_cash(cur_loc)
        self.backtest_df.loc[cur_loc, 'total_capital'] = self.__get_period_total_capital(cur_loc)
        self.backtest_df.loc[cur_loc, 'total_pnl'] = self.__get_period_pnl(cur_loc)
        self.backtest_df.loc[cur_loc, 'drawdown'] = self.__get_period_mdd(cur_loc)
        self.temp_entry_price = self.backtest_df.loc[cur_loc, 'executed_price']
        self.backtest_df.loc[cur_loc, 'return_of_single_trade'] = -self.__get_cur_single_abs_return(cur_loc)
        return None

    def __exit_long_signal(self, cur_loc):
        self.backtest_df.loc[cur_loc:, 'current_position'] = None
        self.backtest_df.loc[cur_loc, 'exit_signal'] = 'EXIT_LONG'
        self.backtest_df.loc[cur_loc, 'executed_price'] = self.__get_exit_executed_price(cur_loc)
        self.backtest_df.loc[cur_loc, 'change_in_shares'] = self.__get_exit_shares_calc(cur_loc, 'EXIT_LONG')
        self.backtest_df.loc[cur_loc:, 'position_shares'] += self.backtest_df.loc[cur_loc, 'change_in_shares']
        self.backtest_df.loc[cur_loc, 'position_value'] = self.__get_position_value(cur_loc)
        self.backtest_df.loc[cur_loc, 'cost_and_slippage'] = self.__get_slippage_trading_cost(cur_loc)
        self.backtest_df.loc[cur_loc:, 'cash'] += self.__get_chg_in_cash(cur_loc)
        self.backtest_df.loc[cur_loc:, 'cash'] = self.__get_interest_cash(cur_loc)
        self.backtest_df.loc[cur_loc, 'total_capital'] = self.__get_period_total_capital(cur_loc)
        self.backtest_df.loc[cur_loc, 'total_pnl'] = self.__get_period_pnl(cur_loc)
        self.backtest_df.loc[cur_loc, 'drawdown'] = self.__get_period_mdd(cur_loc)
        self.backtest_df.loc[cur_loc, 'return_of_single_trade'] = self.__get_cur_single_abs_return(cur_loc, 'exit')
        return None

    def __exit_short_signal(self, cur_loc):
        self.backtest_df.loc[cur_loc:, 'current_position'] = None
        self.backtest_df.loc[cur_loc, 'exit_signal'] = 'EXIT_SHORT'
        self.backtest_df.loc[cur_loc, 'executed_price'] = self.__get_exit_executed_price(cur_loc)
        self.backtest_df.loc[cur_loc, 'change_in_shares'] = self.__get_exit_shares_calc(cur_loc, 'EXIT_SHORT')
        self.backtest_df.loc[cur_loc:, 'position_shares'] += self.backtest_df.loc[cur_loc, 'change_in_shares']
        self.backtest_df.loc[cur_loc, 'position_value'] = self.__get_position_value(cur_loc)
        self.backtest_df.loc[cur_loc, 'cost_and_slippage'] = self.__get_slippage_trading_cost(cur_loc)
        self.backtest_df.loc[cur_loc:, 'cash'] += self.__get_chg_in_cash(cur_loc)
        self.backtest_df.loc[cur_loc:, 'cash'] = self.__get_interest_cash(cur_loc)
        self.backtest_df.loc[cur_loc, 'total_capital'] = self.__get_period_total_capital(cur_loc)
        self.backtest_df.loc[cur_loc, 'total_pnl'] = self.__get_period_pnl(cur_loc)
        self.backtest_df.loc[cur_loc, 'drawdown'] = self.__get_period_mdd(cur_loc)
        self.backtest_df.loc[cur_loc, 'return_of_single_trade'] = -self.__get_cur_single_abs_return(cur_loc, 'exit')
        return None

    def __update_position(self, cur_loc):
        self.backtest_df.loc[cur_loc, 'position_value'] = self.__get_position_value(cur_loc)
        self.backtest_df.loc[cur_loc:, 'cash'] = self.__get_interest_cash(cur_loc)
        self.backtest_df.loc[cur_loc, 'total_capital'] = self.__get_period_total_capital(cur_loc)
        self.backtest_df.loc[cur_loc, 'total_pnl'] = self.__get_period_pnl(cur_loc)
        self.backtest_df.loc[cur_loc, 'drawdown'] = self.__get_period_mdd(cur_loc)
        if self.backtest_df.loc[cur_loc, 'current_position'] == 'LONG':
            self.backtest_df.loc[cur_loc, 'return_of_single_trade'] = self.__get_cur_single_abs_return(cur_loc)
        elif self.backtest_df.loc[cur_loc, 'current_position'] == 'SHORT':
            self.backtest_df.loc[cur_loc, 'return_of_single_trade'] = -self.__get_cur_single_abs_return(cur_loc)
    
    def __get_traded_wo_cost(self, cur_loc):
        return - self.backtest_df.loc[cur_loc, 'change_in_shares'] * self.backtest_df.loc[cur_loc, 'executed_price']

    def __get_interest_cash(self, cur_loc):
        return np.exp(self.interest_rate / 252) * self.backtest_df.loc[cur_loc, 'cash']

    def __get_period_total_capital(self, cur_loc):
        return self.backtest_df.loc[cur_loc, 'cash'] + self.backtest_df.loc[cur_loc, 'position_value']

    def __get_slippage_trading_cost(self, cur_loc):
        total_percent_cost = np.random.normal(self.mean_slippage, self.mean_slippage/10) + self.trading_cost
        traded = self.__get_traded_wo_cost(cur_loc)
        return abs(traded * total_percent_cost)

    def __get_chg_in_cash(self, cur_loc):
        traded = self.__get_traded_wo_cost(cur_loc)
        return traded - self.backtest_df.loc[cur_loc, 'cost_and_slippage']

    def __get_entry_shares_calc(self, cur_loc, long_short):
        if long_short == 'LONG':
            long_short = 1
        elif long_short == 'SHORT':
            long_short = -1
        total_cash = self.backtest_df.loc[cur_loc, 'cash']
        if self.entry_amount_type == 'ALL_IN':
            return long_short * total_cash / self.backtest_df.loc[cur_loc, 'executed_price']

    def __get_exit_shares_calc(self, cur_loc, exit_long_short):
        if self.exit_amount_type == 'ALL_OUT':
            if exit_long_short == 'EXIT_LONG':
                return - self.backtest_df.loc[cur_loc, 'position_shares']
            if exit_long_short == 'EXIT_SHORT':
                return - self.backtest_df.loc[cur_loc, 'position_shares']
            return

    def __get_entry_executed_price(self, cur_loc):
        if self.trade_timing == 'at_close':
            return self.backtest_df.loc[cur_loc, 'close']

    def __get_exit_executed_price(self, cur_loc):
        if self.exit_timing == 'at_close':
            return self.backtest_df.loc[cur_loc, 'close']

    def __get_position_value(self, cur_loc):
        return self.backtest_df.loc[cur_loc, 'position_shares'] * self.backtest_df.loc[cur_loc, 'close']

    def __get_period_pnl(self, cur_loc):
        return self.backtest_df.loc[cur_loc, 'total_capital'] / self.initial_capital - 1

    def __get_period_mdd(self, cur_loc):
        prev_max = self.backtest_df.loc[:cur_loc, 'total_capital'].max()
        cur_value = self.backtest_df.loc[cur_loc, 'total_capital']
        return cur_value / prev_max - 1

    def __get_cur_single_abs_return(self, cur_loc, exit_entry=None):
        if exit_entry == 'exit':
            cur_price = self.backtest_df.loc[cur_loc, 'executed_price']
        else:
            cur_price = self.backtest_df.loc[cur_loc, 'close']
        return cur_price / self.temp_entry_price - 1

    def __get_cumulative_return(self):
        end_val = self.backtest_df.loc[self.backtest_df.shape[0] - 1, 'total_capital']
        return end_val / self.initial_capital - 1

    def __get_annualised_return(self):
        periods = self.backtest_df.shape[0] / 252
        cum_ret = self.__get_cumulative_return()
        return (cum_ret+1)**(1 / periods) - 1

    def __get_daily_volatility(self):
        return self.backtest_df.loc[:, 'daily_return'].std()

    def get_annualised_volatility(self):
        return self.__get_daily_volatility() * (252**0.5)

    def __get_mdd(self):
        return self.backtest_df.loc[:, 'drawdown'].min()

    def __get_annualised_sharpe(self):
        return (self.__get_annualised_return() - self.interest_rate) / self.get_annualised_volatility()

    def __get_sortino(self):
        temp_array = self.backtest_df.loc[:, 'daily_return']
        return (self.__get_annualised_return() - self.interest_rate) / temp_array.loc[temp_array < 0].std()

    def __get_calmer(self):
        return self.__get_annualised_return() / self.__get_mdd()


def print_dict(target_dict):
    for i in target_dict:
        print(i.ljust(27) + ': ' + str(round(target_dict[i], 5)))
    return None





    








