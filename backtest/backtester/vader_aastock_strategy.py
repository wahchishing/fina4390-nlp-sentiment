import numpy as np
import pandas as pd

####################################################################################################################################################
#
#   This script is an example to show how the required functions should be structured
#
#   1. bar_size
#   return the number of rows you need when you process a signal, i.e. number of rows needed in target_strategy_function
#
#   2. price_preprocessing
#   pre-process the price data
#   Add columns that you need to use when the strategy generates signals
#
#   3. target_strategy_function
#   Generate buy/sell signals
#   The function should have input input_df
#   The function should return 4 different signals
#   'LONG', 'SHORT', 'EXIT_LONG', 'EXIT_SHORT', and None
#   'EXIT' depends on the input cur_pos
#
#   Columns in raw price df: date_time, open, high, low, close, adjclose, volume
#
####################################################################################################################################################


def bar_size():
    return 5


def price_preprocessing(input_df):
    return_df = input_df.copy()
    score_df = pd.read_csv('/Users/anthonyWCS/Desktop/FINA4390_Group_Project/fina4390-nlp-sentiment/data/aastock/aastock_HSI_Daily_score_09:00_16:00.csv').reset_index(drop=True)
    return_df.loc[:, 'news_content_score'] = score_df.loc[score_df.loc[:, 'news_date'].isin(return_df.loc[:, 'date_time']), 'total_vader_score_tile']
    return_df.loc[:, '5D_score_SMA'] = return_df.loc[:, 'news_content_score'].rolling(window=5).mean()
    return return_df


def target_strategy_function(input_df):
    index_list = input_df.index.tolist()
    cur_pos = input_df.loc[index_list[-1], 'current_position']
    if np.isnan(input_df.loc[index_list[0], '5D_SMA']) or np.isnan(input_df.loc[index_list[0], '10D_SMA']):
        return None
    if cur_pos is None:
        if input_df.loc[index_list[0], 'news_content_score'] >= input_df.loc[index_list[0], '5D_score_SMA'] and input_df.loc[index_list[0], 'news_content_score'] > 0:
            return 'LONG'
        elif input_df.loc[index_list[0], 'news_content_score'] <= input_df.loc[index_list[0], '5D_score_SMA'] and input_df.loc[index_list[0], 'news_content_score'] < 0:
            return 'SHORT'
        else:
            return None
    elif cur_pos == 'LONG':
        if input_df.loc[index_list[0], '5D_score_SMA'] > input_df.loc[index_list[0], 'news_content_score']:
            return 'EXIT_LONG'
        else:
            return None
    elif cur_pos == 'SHORT':
        if input_df.loc[index_list[0], '5D_score_SMA'] <= input_df.loc[index_list[0], 'news_content_score']:
            return 'EXIT_SHORT'
        else:
            return None
