from backtest.backtester.backtest_helper import *
import json
from backtest.backtester.strategies import *

####################################################################################################################################################
#
#   This script is the main runner for backtest
#
#   Just refer to the example below, I am sleepy lol
#
####################################################################################################################################################


'''
Required parameters:
    stock_code
    start_date
    end_date
    preprocess_func
    strategy_func


Optional parameters:
    initial=1000000
    bet_size_type='ALL_IN'
    exit_size_type='ALL_OUT'
    entry_trade_timing='at_close'
    exit_trade_timing='at_close'
    cost_factor=0.0015
    slippage_mean=0.001
    interest=0.0
'''


def dict_to_json(input_json):
    return_json = json.dumps(input_json, sort_keys=True, indent=4)
    return return_json


if __name__ == '__main__':
    target_stock = '700.HK'
    test_start = '2015-05-09'
    test_end = '2019-05-09'
    pre_process = price_preprocessing
    strategy_function = target_strategy_function
    input_bar_size = bar_size()

    runner = BacktestingHelper(stock_code=target_stock,
                               start_date=test_start,
                               end_date=test_end,
                               preprocess_func=pre_process,
                               strategy_func=strategy_function)
    runner.run_backtest(test_bar_size=input_bar_size)
    trade_records = runner.get_trade_records()
    print(dict_to_json(trade_records))
    runner.plot_graph()

