import numpy as np

####################################################################################################################################################
#
#   This script is an example to show how the required functions should be structured
#
#   1. bar_size
#   return the number of rows you need when you process a signal, i.e. number of rows needed in target_strategy_function
#
#   2. price_preprocessing
#   pre-process the price data
#   Add columns that you need to use when the strategy generates signals
#
#   3. target_strategy_function
#   Generate buy/sell signals
#   The function should have input input_df
#   The function should return 4 different signals
#   'LONG', 'SHORT', 'EXIT_LONG', 'EXIT_SHORT', and None
#   'EXIT' depends on the input cur_pos
#
#   Columns in raw price df: date_time, open, high, low, close, adjclose, volume
#
####################################################################################################################################################


def bar_size():
    return 1


def price_preprocessing(input_df):
    return_df = input_df.copy()
    return_df.loc[:, '5D_SMA'] = return_df.loc[:, 'close'].rolling(window=5).mean()
    return_df.loc[:, '10D_SMA'] = return_df.loc[:, 'close'].rolling(window=10).mean()
    return_df.loc[:, '10D_SMA'] = return_df.loc[:, 'close'].rolling(window=10).mean()
    return return_df


def target_strategy_function(input_df):
    index_list = input_df.index.tolist()
    cur_pos = input_df.loc[index_list[-1], 'current_position']
    if np.isnan(input_df.loc[index_list[0], '5D_SMA']) or np.isnan(input_df.loc[index_list[0], '10D_SMA']):
        return None
    if cur_pos is None:
        if input_df.loc[index_list[0], '5D_SMA'] <= input_df.loc[index_list[0], '10D_SMA']:
            return 'LONG'
        elif input_df.loc[index_list[0], '5D_SMA'] > input_df.loc[index_list[0], '10D_SMA']:
            return 'SHORT'
        else:
            return None
    elif cur_pos == 'LONG':
        if input_df.loc[index_list[0], '5D_SMA'] > input_df.loc[index_list[0], '10D_SMA']:
            return 'EXIT_LONG'
        else:
            return None
    elif cur_pos == 'SHORT':
        if input_df.loc[index_list[0], '5D_SMA'] <= input_df.loc[index_list[0], '10D_SMA']:
            return 'EXIT_SHORT'
        else:
            return None