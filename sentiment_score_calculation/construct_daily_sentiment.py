import pandas as pd
from data_access.util_functions import *
import timeit
from data_access.get_stock_price_yahoo import get_hk_stock_price
from sentiment_score_calculation.vader_sentiment import *
from sentiment_score_calculation.word_count_sentiment import CountScoreHelper
import pickle
import os


PROJECT_DIR = os.path.abspath(__file__)[:os.path.abspath(__file__).find('nlp-sentiment') + 13]


class DailySentimentHelper:
    def __init__(self):
        pass

    def construct_daily_sentiment(self, score_helper, start_time, end_time):
        log_message('Start getting weight and df')
        weight = pd.read_csv(os.path.join(PROJECT_DIR, 'data/bloomberg/HSI_weighting.csv'))
        weight.set_index('date', inplace=True)
        df = pd.read_csv(os.path.join(PROJECT_DIR, 'data/aastock/aastock_HSI_m.csv'))
        df.stock_code = df.stock_code.apply(lambda x: "{:05d}".format(x))
        df.dropna(subset=['news_content'], inplace=True)
        weight_dict = {stock_code: weight[stock_code]
                       for stock_code in weight.columns}
        df['weight'] = [weight_dict.get(
            code).loc[date]/100 for date, code in zip(df['news_date'], df['stock_code'])]
        log_message('Start calculating score')
        df['score_title'] = score_helper.get_sentiment_scores(df['title'])
        df['score_content'] = score_helper.get_sentiment_scores(
            df['news_content'])
        log_message('Start constructing daily version')
        start_date = pd.to_datetime(df['news_time'].iloc[-1]).date()
        end_date = pd.to_datetime(df['news_time'].iloc[0]).date()
        score_list = []
        for date in pd.date_range(start_date, end_date):
            if start_time >= end_time:
                opening = str(date.date()-pd.Timedelta(1, unit='d')
                              ) + ' ' + start_time
                closing = str(date.date()) + ' ' + end_time
            else:
                opening = str(date.date()) + ' ' + start_time
                closing = str(date.date()) + ' ' + end_time
            tmp = df.loc[(df['news_time'] > opening) &
                         (df['news_time'] <= closing)]
            if len(tmp) == 0:
                score_title = None
                score_content = None
            else:
                score_title = sum(tmp['weight'] * tmp['score_title'])
                score_content = sum(tmp['weight'] * tmp['score_content'])
            score_list.append({'date_time': str(date.date()),
                               'score_title': score_title,
                               'score_content': score_content,
                               'number_of_news': len(tmp),
                               'weighted_number_of_news': sum(tmp['weight']),
                               })
        df = pd.DataFrame(score_list)
        price = get_hk_stock_price('HSI')
        price.drop('adjclose', inplace=True, axis=1)
        price.columns = ['date_time', 'HSI_open',
                         'HSI_high', 'HSI_low', 'HSI_close', 'HSI_volume']
        df = pd.merge(df, price, on='date_time')
        return df

    def merge_non_trading_date(self, df):
        df_revised = df.dropna(
            subset=['score_content', 'HSI_close'], how='all')
        df_revised = df_revised.reset_index(drop=True)
        col = ['score_title', 'score_content',
               'number_of_news', 'weighted_number_of_news']
        for i in df_revised[df_revised['HSI_close'].isnull()].index:
            for c in col:
                df_revised.loc[i + 1, c] += df_revised.loc[i, c]
        df_revised = df_revised.dropna(axis=0, subset=['HSI_close'])
        return df_revised


if __name__ == "__main__":
    sh = VaderScoreHelper()
    # sh = CountScoreHelper()
    # with open('count_full.pickle', 'rb') as f:
    #     d = pickle.load(f)
    #     sh.load_prob_dict(d)
    # sh.update_word_list(pos_count=40,neg_count=40)
    log_message('Start')
    dh = DailySentimentHelper()
    log_message('Start constructing daily sentiment')
    df = dh.construct_daily_sentiment(sh, '16:00', '16:00')
    log_message('Start merging non trading date')
    df = dh.merge_non_trading_date(df)
    log_message('Start exporting csv')
    df.to_csv(os.path.join(PROJECT_DIR, 'data/aastock/vader_HSI_score_1600_1600_m.csv'), index=False)
