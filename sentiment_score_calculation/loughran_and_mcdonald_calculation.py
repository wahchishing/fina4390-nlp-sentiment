import pandas as pd
import os
import time
import nltk
from data_access.util_functions import *

path_to_file = os.path.dirname(os.path.realpath(__file__))
path_to_lm_data = os.path.join(os.path.dirname(path_to_file), 'data/LoughranMcDonald_MasterDictionary_2018.csv')


class LoughranMcdonaldHelper:
    def __init__(self):
        log_message('Reading LM dictionary')
        self.df = pd.read_csv(path_to_lm_data)
        self.df.dropna(inplace=True)
        self.df.loc[:, 'Word'] = self.df.loc[:, 'Word'].apply(lambda x: x.lower())
        self.available_score_type = ['Negative', 'Positive',
       'Uncertainty', 'Litigious', 'Constraining', 'Superfluous',
       'Interesting', 'Modal']
        log_message('LM sentiment score ready with available score types as below:')
        print(self.available_score_type)

    def get_sentiment_score_lm(self, text_list, score_type_list=None):
        """
        Get sentiment score from LM dictionary
        :param text_list:
        :param score_type_list:
        :return:
        """
        if type(text_list) == str:
            text_list = [text_list]
        if score_type_list is None:
            score_type_list = ['Positive', 'Negative']
        if type(score_type_list) == str:
            score_type_list = [score_type_list]
        text_list = [text.lower() for text in text_list]

        # if you have can you can try this, but got error of missing resource from my side
        # text_list = [nltk.word_tokenize(text.lower()) for text in text_list]

        return_dict = {text: {score_type: self.df.loc[self.df['Word'] == text, score_type].values[0] for score_type in score_type_list} for text in text_list}
        return return_dict







