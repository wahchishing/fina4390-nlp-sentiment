import numpy as np
import pandas as pd
import pickle
from collections import Counter
import nltk
from data_access.util_functions import log_message
import datetime
import pickle
from data_access.get_stock_price_yahoo import get_hk_stock_price


class CountScoreHelper:
    def __init__(self):
        self.prob_dict = None
        self.pos_word = None
        self.neg_word = None
        self.download = False
        self.min_word_count = 5
        self.pos_count=30
        self.pos_prob=0.55
        self.neg_count=30
        self.neg_prob=0.45

    def train(self, news_content, returns):
        log_message('Tokenizing News Contents')
        word_count = self._get_word_count(news_content)
        returns = returns.reset_index(drop=True)
        direction = np.sign(returns)
        log_message('Training...')
        pos_tmp = word_count.loc[direction == 1]
        pos_tmp = pos_tmp.sum()
        neg_tmp = word_count.loc[direction == -1]
        neg_tmp = neg_tmp.sum()
        total_tmp = pos_tmp + neg_tmp
        self.prob_dict = {
            k: (pos_tmp[k]/total_tmp[k], total_tmp[k]) for k in total_tmp.keys()}
        self.pos_word = {k: v for k, v in self.prob_dict.items() if (
            v[1] > self.pos_count) & (v[0] > self.pos_prob)}
        self.neg_word = {k: v for k, v in self.prob_dict.items() if (
            v[1] > self.neg_count) & (v[0] < self.neg_prob)}
        log_message('Training Finished!')
        log_message(
            f'Length of positive : negative: {len(self.pos_word)} : {len(self.neg_word)}')

    def update_word_list(self, pos_count=None, pos_prob=None, neg_count=None, neg_prob=None):
        if pos_count is not None:
            self.pos_count = pos_count
        if neg_count is not None:
            self.neg_count = neg_count
        if pos_prob is not None:
            self.pos_prob = pos_prob
        if neg_prob is not None:
            self.neg_prob = neg_prob      
        if self.prob_dict is None:
            log_message(
                'ERROR: Probability Dictionary not exists! Please train or load prob dict first.')
            return None
        self.pos_word = {k: v for k, v in self.prob_dict.items() if (
            v[1] > self.pos_count) & (v[0] > self.pos_prob)}
        self.neg_word = {k: v for k, v in self.prob_dict.items() if (
            v[1] > self.neg_count) & (v[0] < self.neg_prob)}
        log_message(
            f'Length of positive : negative: {len(self.pos_word)} : {len(self.neg_word)}')

    def get_sentiment_score(self, word_counter):
        s = 0
        p = 0
        n = 0
        cnt = 0
        for k in word_counter:
            if k in self.pos_word:
                cnt += 1
                p += word_counter[k] * \
                    self._transform_prob(self.pos_word[k][0])
            if k in self.neg_word:
                cnt += 1
                n -= word_counter[k] * \
                    self._transform_prob(self.neg_word[k][0])
        if (p == 0 and n == 0) or (cnt <= self.min_word_count):
            s = 0
        else:
            s = (p-n)/(p+n)
        return s

    def get_sentiment_scores(self, target_texts_list):
        if (self.pos_word is None) or (self.neg_word is None):
            log_message('ERROR: Word List is empty. Please train or load model')
            return None
        word_counter_list = self._get_word_count(target_texts_list)
        return  [self.get_sentiment_score(word_counter) for word_counter in word_counter_list]

    def save_prob_dict(self, pickle_path):
        if self.prob_dict is not None:
            with open(pickle_path, 'wb') as f:
                pickle.dump(self.prob_dict, f)
            log_message('Probability Dictionary saved.')
        else:
            log_message("ERROR: No probability dictionary trained.")

    def load_prob_dict(self, prob_dict):
        self.prob_dict = prob_dict
        self.update_word_list()

    def _get_word_count(self, news_content):
        if not self.download:
            nltk.download('punkt')
            self.download = True
        news_content = news_content.apply(lambda x: x.lower())
        tokenizer = nltk.tokenize.RegexpTokenizer(r'\w+')
        word_count = [Counter(filter(lambda x: x.lower() != x.upper(), set(
            tokenizer.tokenize(nc)))) for nc in news_content]
        word_count = pd.Series(word_count)
        return word_count

    def _transform_prob(self,x):
        return (x-.5)/(np.sqrt((x-0.5)**2+0.1))

if __name__ == '__main__':
    df = pd.read_csv('data/aastock/expanded_aastock_HSI.csv')
    df['news_time'] = pd.to_datetime(df['news_time'])
    # df = df[df['news_time']<'2018']
    def get_return(news_time, price):
        if news_time.time() < datetime.time(16, 0):
            begin_date = news_time.date()
        else:
            begin_date = news_time.date() + datetime.timedelta(1)
        tmp = price.loc[price.date_time >= str(begin_date), 'adjclose']
        if len(tmp) < 2:
            return 0
        else:
            return (tmp.shift(-1)/tmp - 1).iloc[0]
    df['stock_code'] = df['stock_code'].apply(lambda sc: format(sc, '04') + ".HK")
    stock_code = set(df['stock_code'])
    for sc in stock_code:
        tmp = df[df['stock_code'] == sc]['news_time']
        end_date = str(tmp.iloc[0].date() + datetime.timedelta(2))
        start_date = str(tmp.iloc[-1].date() - datetime.timedelta(7))
        try:
            price = get_hk_stock_price(sc, start_date, end_date)
            ind = (df['stock_code'] == sc)
            df.loc[ind, 'return'] = [get_return(
                nt, price) for nt in df.loc[ind, 'news_time']]
        except:
            continue

    df['direction'] = np.sign(df['return'])
    df.dropna(subset=['return'], inplace=True)
 
    csh = CountScoreHelper()
    csh.train(df['news_content'], df['return'])
    csh.update_word_list(pos_count=40,neg_count=40)
    csh.save_prob_dict('count_full.pickle')
    with open('count_full.pickle', 'rb') as f:
        d = pickle.load(f)
        csh.load_prob_dict(d)