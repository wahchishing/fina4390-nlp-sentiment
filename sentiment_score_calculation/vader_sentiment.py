from vaderSentiment.vaderSentiment import *


class VaderScoreHelper:
    def __init__(self):
        self.analyzer = SentimentIntensityAnalyzer()
        self.score_type = 'compound'
    def update_lexicon(self, new_words_dict):
        self.analyzer.lexicon.update(new_words_dict)

    def get_sentiment_score(self, target_text, score_type=None):
        result_scores = self.analyzer.polarity_scores(target_text)
        if score_type is None:
            return result_scores
        else:
            return result_scores[score_type]

    def get_sentiment_scores(self, target_texts_list):
        return [self.analyzer.polarity_scores(text)[self.score_type] for text in target_texts_list]


if __name__ == '__main__':
    testing = VaderScoreSHelper()
    target_text = "APPLE is good!"
    score = testing.get_sentiment_score(target_text)
    print(score)
